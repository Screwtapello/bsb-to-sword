case "$1" in
    installable-module/bsb.zip)
        redo-ifchange usable-module/mods.d/bsb.conf

        mkdir -p installable-module

        ( cd usable-module && zip -9 -r - mods.d/bsb.conf modules/ ) >"$3"
        ;;
    submittable-module/bsb.zip)
        redo-ifchange ../sources/bsb.conf ../build/bsb.osis.xml

        mkdir -p submittable-module

        zip -9 -j "$3" ../sources/bsb.conf ../build/bsb.osis.xml >&2
        ;;
    *)
        echo "Unknown target $1" >&2
        exit 1
esac
