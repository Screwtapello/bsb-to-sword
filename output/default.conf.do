case "$1" in
    # This file is what we depend on to represent "all the files in a module",
    # since redo isn't great at handling commands that build a directory instead
    # of a single file.
    usable-module/mods.d/bsb.conf)
        redo-ifchange \
            ../sources/bsb.conf \
            ../build/bsb.osis.xml \
            ../sources/emptyvss-expected.txt

        # Clear out all the "side-effect" files that redo isn't tracking for us.
        rm -rf \
            usable-module/modules/texts/ztext/bsb \
            test-module/mods.d \
            test-module/modules/texts/ztext/bsb

        # Construct the output directories
        mkdir -p \
            usable-module/mods.d \
            usable-module/modules/texts/ztext/bsb \
            test-module/mods.d \
            test-module/modules/texts/ztext/bsb

        # Stage our output into the test-module directory.
        cp ../sources/bsb.conf test-module/mods.d/
        osis2mod test-module/modules/texts/ztext/bsb/ \
            ../build/bsb.osis.xml -z z -l 9 1>&2

        # Generate the list of empty verses in this module.
        ( cd test-module && emptyvss BSB ) > test-module/emptyvss.txt

        # Compare it against the list of verses we expect to be empty.
        diff -urw \
            test-module/emptyvss.txt \
            ../sources/emptyvss-expected.txt 1>&2

        # If we got this far, everything seems to be fine.
        # Let's move the side-effect files to their final location.
        mv test-module/modules/texts/ztext/bsb/* \
            usable-module/modules/texts/ztext/bsb/

        # Copy the conf file into a temp file, so redo can move it into place.
        mv test-module/mods.d/bsb.conf "$3"

        # Remove the now-empty staging directory
        rm test-module/emptyvss.txt
        rmdir test-module/mods.d
        rmdir -p test-module/modules/texts/ztext/bsb/

        ;;
    *)
        echo "Unknown target $1" >&2
        exit 1
esac
