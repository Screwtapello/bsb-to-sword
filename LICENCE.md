The files in the `sources/bsb_usfm` directory
and the file `sources/bsb_tables.xlsx` are under
the Creative Commons Zero licence.

The files in the `script/adyeths/u2o` directory are under the UNLICENCE.

The files in the `script/Screwtapello/osis-decorators/` directory
are under the GPLv2 (or at your option, any later version).

The files in the `script/dilshod/xlsx2csv/` directory
are under the MIT licence.

All other files in this repository are under the ISC licence:

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
PERFORMANCE OF THIS SOFTWARE.
