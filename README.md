Berean Standard Bible as a SWORD Project module
===============================================

The [Berean Standard Bible][BSB] is
a completely new English translation of the Holy Bible
based on the best available manuscripts and sources.
Each word is connected back to Greek or Hebrew
to produce a transparent text that can be studied for its root meanings.

Unlike other modern translations,
it has been placed in the Public Domain
so it can be freely used and distributed.

The [SWORD Project][SWORD] creates Bible software
that is cross-platform and open-source,
so it can be freely used and distributed.

It seemed to me that these two things would go well together.

[BSB]: https://berean.bible/
[SWORD]: http://www.crosswire.org/sword/

This repository contains the BSB in USFM format,
along with conversion scripts required to convert it
into a SWORD Project module.

You can download pre-built versions of this module:

  - [The latest in-development version](https://gitlab.com/api/v4/projects/46592514/jobs/artifacts/main/raw/output/installable-module/bsb.zip?job=build)

Requirements
------------

  - A Linux or BSD based operating system
  - Python 3
  - The SWORD project conversion tools
  - The Info-Zip `zip` tool
  - The libxml `xmllint` tool

On a Debian or Ubuntu system, this should install all the necessary packages:

    sudo apt install python3 libsword-utils zip libxml2-utils

If you want to do any development,
you may wish to also install apenwarr's [redo] build tool
to run the build scripts.
Unlike the provided `do` tool,
`redo` will only rebuild files whose dependencies have changed,
instead of always rebuilding everything from scratch.

[redo]: https://github.com/apenwarr/redo

Building
--------

Run the included `do` file in the root of the repository.

Results
-------

If `do.sh` completes successfully,
a number of subdirectories will be created inside `output/`:

  - `output/usable-module/` is a standard SWORD module directory structure.
    If you set up this path in a libSWORD-based application
    as a source of modules,
    you should be able to install this module.
    Alternatively,
    if you start a libSWORD-based application from this directory,
    it should automatically see this module as already installed.
  - `output/installable-module/` contains the module data in a .ZIP file.
    Some libSWORD applications can install an individual module from
    a .ZIP file, rather than a directory full of modules.
  - `output/submittable-module/` contains the module source in a .ZIP file,
    in the form expected by CrossWire's module submission system.

Updates
-------

The included `script/update-subtrees.sh` script
will update the conversion tools in this repository
to their latest upstream versions.
For externally-provided tools like Python, `zip` and `libsword-utils`,
you'll have to update them yourself.

To do
-----

  - Find a way to incorporate Strongs numbers and grammatical codes,
    as seen in the BSB translation tables.
      - This requires new `<work/>` elements in the OSIS header, like:

            <work osisWork="ohsm"><refSystem>Dict.OHSM</refSystem></work>
            <work osisWork="robinson"><refSystem>Dict.Robinson</refSystem></work>

      - The extra information goes in attributes on the `<w/>` element.
      - The `morph` attribute takes work-prefixed tokens,
        and should be 1-to-1 with Strong's numbers.
        [Robinson's codes] can be used for Greek,
        [OHSM codes] can be used for Hebrew,
        if we can figure out how to convert the codes the BSB uses.
        This also requires the OSISMorph global filter in the conf file.
  - What do {curly brackets} mean in the BSB translation tables?
  - Add markup for words added by the translators,
    denoted by English in square brackets in the translation tables
      - OSIS wants us to distinguish "added" versus "changed" words,
        and I'm not sure we can, so maybe we should leave these alone?
  - Is it the Berean "Standard" Bible, or the Berean "Study" Bible?
      - The website says "Standard" but all the downloaded texts say "Study".
  - Create a "BSBlex" dictionary module?
  - Add `<divineName/>` markup to instances of the Divine Name,
    so we get the nice small-caps formatting.
    We can probably check the KJV to see which Strong's Numbers
    represent the Divine Name.

[Robinson's codes]: https://gitlab.com/crosswire-bible-society/robinson/-/blob/main/robinson.tei.xml
[OHSM codes]: https://github.com/openscriptures/morphhb/tree/master/parsing
