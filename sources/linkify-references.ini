[ReferencePatterns]
# All the ways scripture references may be written in the text.
# The following codes are available:
#
# BOOK         matches one of the book names or abbreviations below.
# CHAPTER      matches a chapter number
# VERSE        matches a verse number
# FROM_CHAPTER matches a chapter number, and means this pattern refers to a
#              span of text. TO_CHAPTER must be used in the same pattern,
#              and must be greater than or equal to FROM_CHAPTER.
# TO_CHAPTER   matches a chapter number, and means this pattern refers to a
#              span of text. FROM_CHAPTER must be used in the same pattern,
#              and must be less than or equal to TO_CHAPTER.
# FROM_VERSE   matches a verse number, and means this pattern refers to a
#              span of text. TO_VERSE must be used in the same pattern,
#              and must be greater than or equal to FROM_VERSE.
# TO_VERSE     matches a verse number, and means this pattern refers to a
#              span of text. FROM_VERSE must be used in the same pattern,
#              and must be less than or equal to TO_VERSE.
#
# NOTE: The first matching pattern in the list will be used.
# If "BOOK CHAPTER:VERSE" comes before "BOOK CHAPTER:FROM_VERSE-TO_VERSE",
# then text like "Matthew 22:37-40" will match "Matthew 22:37" and the "-40"
# will be ignored.

# Absolute references must include BOOK. If they are missing one of the VERSE
# codes, the reference points to the entire chapter. For example, with the
# pattern "BOOK CHAPTER", the reference "Psalm 23" refers to the entire psalm.
# If they are also missing one of the CHAPTER codes, the reference points to the
# entire book. For example, with the pattern "BOOK", the reference "Jude" refers
# to the entire book.
AbsolutePatterns =
    BOOK FROM_CHAPTER:FROM_VERSE–TO_CHAPTER:TO_VERSE
    BOOK FROM_CHAPTER:FROM_VERSE-TO_CHAPTER:TO_VERSE
    BOOK CHAPTER:FROM_VERSE–TO_VERSE
    BOOK CHAPTER:FROM_VERSE-TO_VERSE
    BOOK CHAPTER:VERSE
    BOOK FROM_CHAPTER–TO_CHAPTER
    BOOK FROM_CHAPTER-TO_CHAPTER
    BOOK CHAPTER

# Location-relative references must *not* include BOOK, and will be intepreted
# relative to the book where they appear. For example, with the pattern "chapter
# CHAPTER", the reference "chapter 5" in Matthew 2:3 refers to Matthew 5. If the
# pattern is also missing a CHAPTER code, it will be interpreted relative to the
# chapter where the reference appears. For example, with the pattern "verses
# FROM_VERSE-TO_VERSE", the reference "verses 17-23" in a footnote of
# Matthew 2:3 refers to Matthew 2:17-23.
LocationRelativePatterns =
    chapters FROM_CHAPTER–TO_CHAPTER
    chapters FROM_CHAPTER-TO_CHAPTER
    chapters CHAPTER
    chapter CHAPTER
    Verses FROM_VERSE–TO_VERSE
    Verses FROM_VERSE-TO_VERSE
    verses FROM_VERSE–TO_VERSE
    verses FROM_VERSE-TO_VERSE
    verses VERSE
    verse VERSE

# Reference-relative references also must not include BOOK, and will be
# interpreted relative to the last reference that occurred. For example, with
# the absolute pattern "BOOK CHAPTER:VERSE" and the reference-relative pattern
# "Idem CHAPTER:VERSE", the references "See Genesis 2:3 and later Idem 5:4" will
# be understood as referring to Genesis 2:3 and Genesis 5:4, regardless of where
# the reference appears.
#
# For these patterns, additional codes are available:
#
# NUMBER       The same as VERSE if the previous reference included one,
#              otherwise the same as CHAPTER.
# FROM_NUMBER  matches a verse or chapter number (see NUMBER),
#              and means this pattern refers to a span of text.
#              TO_NUMBER must be used in the same pattern,
#              and must be greater than or equal to FROM_NUMBER.
# TO_NUMBER    matches a verse or chapter number (see NUMBER),
#              and means this pattern refers to a span of text.
#              FROM_NUMBER must be used in the same pattern,
#              and must be less than or equal to TO_NUMBER .
#
ReferenceRelativePatterns =
    Idem CHAPTER:VERSE
    Id CHAPTER:VERSE

# Reference chains are collections of reference-relative references that appear
# close together, Because they appear close together, we can use patterns like
# "a number" - in arbitrary text that would probably *not* be a reference, but
# as part of a reference chain it probably is.
#
# A reference chain has the following parts:
#
#  1. An absolute, location-relative, or reference-relative reference,
#     immediately followed by
#  2. One of the patterns listed in ReferenceChainLinks,
#     immediately followed by
#  3. One of the patterns listed in ReferenceChainItems
#  4. Optionally, this may be followed by more links and items
#     in immediate succession
#
# Once we fail to match a Link or an Item, the chain is over.
# For example, consider the chain "Ezekiel 20:11, 13, and 21."
#
#  1. "Ezekiel 20:11" is an initial reference (absolute, in this case)
#  2. "," matches a link pattern
#  3. "13" matches an item pattern
#  4. ", and" matches a link pattern
#  5. "21" matches an item pattern"
#  6. "." does not match a link pattern, so the chain ends.
#
ReferenceChainLinks =
    # These links allow chains like "verses 7, 8, 14, 15, 17, and 20"
    , and
    ,
    # This link allows chains like "chapters 18 and 19"
    and
ReferenceChainItems =
    # Used in a footnote in 1Cor.5.12:
    #     Deuteronomy 13:5, 17:7, 19:19, 21:21, 22:21, 22:24, and 24:7
    CHAPTER:VERSE
    # Used in many places, including footnotes for the term "Maskil":
    #     Psalms 32, 42, 44–45, 52–55, 74, 78, 88–89, and 142.
    # ...and verse lists:
    #     see also Ezekiel 20:11, 13, and 21.
    FROM_NUMBER–TO_NUMBER
    FROM_NUMBER-TO_NUMBER
    NUMBER

[BookNames]
# List all the aliases and abbreviations used for each book of scripture
# The "official" abbreviation for each book comes from Appendix C of the
# OSIS 2.1.1 User's Manual.
#
# There can be more than one abbreviation, separated by commas:
#
#     Lam = Lamentations, Lament, Lam, Lm

# Hebrew Bible/Old Testament
Gen = Genesis
Exod = Exodus
Lev = Leviticus
Num = Numbers
Deut = Deuteronomy
Josh = Joshua
Judg = Judges
Ruth = Ruth
1Sam = 1 Samuel
2Sam = 2 Samuel
1Kgs = 1 Kings
2Kgs = 2 Kings
1Chr = 1 Chronicles
2Chr = 2 Chronicles
Ezra = Ezra
Neh = Nehemiah
Esth = Esther
Job = Job
Ps = Psalms, Psalm
Prov = Proverbs
Eccl = Ecclesiastes, Qohelet
Song = Song of Solomon, Song of Songs, Cantlcles, Canticle of Canticles
Isa = Isaiah
Jer = Jeremiah
Lam = Lamentations
Ezek = Ezekiel
Dan = Daniel
Hos = Hosea
Joel = Joel
Amos = Amos
Obad = Obadiah
Jonah = Jonah
Mic = Micah
Nah = Nahum
Hab = Habakkuk
Zeph = Zephaniah
Hag = Haggai
Zech = Zechariah
Mal = Malachi

# New Testament
Matt = Matthew
Mark = Mark
Luke = Luke
John = John
Acts = Acts
Rom = Romans
1Cor = 1 Corinthians
2Cor = 2 Corinthians
Gal = Galatians
Eph = Ephesians
Phil = Philippians
Col = Colossians
1Thess = 1 Thessalonians
2Thess = 2 Thessalonians
1Tim = 1 Timothy
2Tim = 2 Timothy
Titus = Titus
Phlm = Philemon
Heb = Hebrews
Jas = James
1Pet = 1 Peter
2Pet = 2 Peter
1John = 1 John
2John = 2 John
3John = 3 John
Jude = Jude
Rev = Revelation

# Apocrypha and Septuagint
Bar = Baruch
AddDan = Additions to Daniel
PrAzar = Prayer of Azariah, Song of the Three Children
Bel = Bel and the Dragon
SgThree = Song of the Three Young Men
Sus = Susanna
1Esd = 1 Esdras
2Esd = 2 Esdras
AddEsth = Additions to Esther
EpJer = Epistle of Jeremiah
Jdt = Judith
1Macc = 1 Maccabees
2Macc = 2 Maccabees
3Macc = 3 Maccabees
4Macc = 4 Maccabees
PrMan = Prayer of Manasseh
Sir = Sirach, Ecclesiasticus
Tob = Tobit
Wis = Wisdom of Solomon, Wisdom

# All of the following abbreviations come from the CrossWire wiki,
# rather than the OSIS User's Manual:
#
#    http://wiki.crosswire.org/OSIS_Book_Abbreviations
#
# Other Apocrypha/Deuterocanon
EsthGr = Greek Esther
SirP = Sirach Prologue
DanGr = Greek Daniel
AddPs = Psalm 151

# Rahlfs' LXX
Odes = Odes
PssSol = Psalms of Solomon

# Rahlfs' variant books
JoshA = Joshua A
JudgB = Judges B
TobS = Tobit S
SusTh = Susanna θ
DanTh = Daniel θ
BelTh = Bel and the Dragon θ

# Vulgate and other Latin mss.
EpLao = Epistle to the Laodiceans
5Ezra = 5 Ezra
4Ezra = 4 Ezra, Ezra Apocalypse
6Ezra = 6 Ezra
PrSol = Prayer of Solomon
PrJer = Prayer of Jeremiah

# Ethiopian Orthodox Canon
1En = 1 Enoch, Ethiopic Enoch, Ethiopic Apocalypse of Enoch
Jub = Jubilees
4Bar = 4 Baruch, Paraleipomena Jeremiou
1Meq = 1 Meqabyan
2Meq = 2 Meqabyan
3Meq = 3 Meqabyan
Rep = Reproof, Tegsas, Tegsats, Taagsas
AddJer = Additions to Jeremiah, Rest of Jeremiah
PsJos = Pseudo-Josephus, Jossipon, Joseph ben Gorion's Medieval History of the Jews

# Armenian Orthodox Canon Additions
EpCorPaul = Epistle of the Corinthians to Paul
3Cor = 3 Corinthians
WSir = Words of Sirach
PrEuth = Prayer of Euthalius
DormJohn = Dormition of John
JosAsen = Joseph and Asenath
T12Patr = Testaments of the Twelve Patriarchs
T12Patr.TAsh = Testament of Asher
T12Patr.TBenj = Testament of Benjamin
T12Patr.TDan = Testament of Dan
T12Patr.TGad = Testament of Gad
T12Patr.TIss = Testament of Issachar
T12Patr.TJos = Testament of Joseph
T12Patr.TJud = Testament of Judah
T12Patr.TLevi = Testament of Levi
T12Patr.TNaph = Testament of Naphtali
T12Patr.TReu = Testament of Reuben
T12Patr.TSim = Testament of Simeon
T12Patr.TZeb = Testament of Zebulun

# Peshitta/Syriac Orthodox Canon
2Bar = 2 Baruch, Apocalypse of Baruch, Syriac Apocalypse of Baruch
EpBar = Letter of Baruch
5ApocSyrPss = Additional Syriac Psalms, 5 Apocryphal Syriac Psalms
JosephusJWvi = Josephus' Jewish War VI

# Apostolic Fathers
1Clem = 1 Clement
2Clem = 2 Clement
IgnEph = Ignatius to the Ephesians
IgnMagn = Ignatius to the Magnesians
IgnTrall = Ignatius to the Trallians
IgnRom = Ignatius to the Romans
IgnPhld = Ignatius to the Philadelphians
IgnSmyrn = Ignatius to the Smyrnaeans
IgnPol = Ignatius to Polycarp
PolPhil = Polycarp to the Philippians
MartPol = Martyrdom of Polycarp
Did = Didache
Barn = Barnabas
Herm = Shepherd of Hermas
Herm.Mand = Shepherd of Hermas, Mandates
Herm.Sim = Shepherd of Hermas, Similitudes
Herm.Vis = Shepherd of Hermas, Visions
Diogn = Diognetus
AposCreed = Apostles' Creed
PapFrag = Fragments of Papias
RelElders = Reliques of the Elders
QuadFrag = Fragment of Quadratus

# Things that aren't Scripture
#
# Sometimes, a text might refer to a portion of some other text
# using the familiar chapter-and-verse notation. We need to recognise
# those refernces as *not* Scripture references, or else we might
# skip over the unrecognised book-name and recognise the chapter and verse
# as a relative reference within the current book.
NotABook = Jasher
