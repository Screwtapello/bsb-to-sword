redo-ifchange \
    ../script/clean-translation-table-data.py \
    translations.csv

../script/clean-translation-table-data.py -v \
    translations.csv "$3"

# If the output file is unchanged, we don't need to re-run any following tasks.
redo-stamp < "$3"
