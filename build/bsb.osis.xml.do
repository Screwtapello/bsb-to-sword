redo-ifchange \
    bsb-cross-refs.osis.xml \
    ../sources/osisCore.2.1.1-cw6.xsd

cp bsb-cross-refs.osis.xml "$3"

xmllint --noout --schema ../sources/osisCore.2.1.1-cw6.xsd "$3"
