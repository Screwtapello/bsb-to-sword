redo-ifchange \
    ../script/Screwtapello/osis-decorators/linkify-references/linkify-references.py \
    bsb-interlinear.osis.xml \
    ../sources/linkify-references.ini

../script/Screwtapello/osis-decorators/linkify-references/linkify-references.py \
    bsb-interlinear.osis.xml ../sources/linkify-references.ini "$3"

# Don't mark this as changed if the content is the same.
redo-stamp < "$3"
