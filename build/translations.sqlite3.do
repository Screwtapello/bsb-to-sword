redo-ifchange translations.csv

sqlite3 -cmd "
create table translations(
    he_ordinal NUMERIC,
    el_ordinal NUMERIC,
    en_ordinal NUMERIC PRIMARY KEY,
    language TEXT,
    verse_ordinal NUMERIC,
    source_word TEXT,
    ignored1 TEXT,
    transliteration TEXT,
    grammar_code TEXT,
    grammar_description TEXT,
    strongs_number NUMERIC,
    reference TEXT,
    heading TEXT,
    cross_references TEXT,
    translation TEXT,
    footnote TEXT,
    gloss TEXT,
    ignored2 TEXT,
    ignored3 TEXT,
    ignored4 TEXT,
    ignored5 TEXT,
    ignored6 TEXT,
    ignored7 TEXT,
    ignored8 TEXT,
    ignored9 TEXT,
    ignored10 TEXT,
    ignored11 TEXT
);
create index translations_he_ordinal on translations(he_ordinal);
create index translations_el_ordinal on translations(el_ordinal);
create index translations_language on translations(language);
create index translations_verse_ordinal on translations(verse_ordinal);
create index translations_strongs_number on translations(strongs_number);
" \
-cmd ".import --csv --skip 2 '|grep -v ^,,,, translations.csv' translations" \
"$3"
