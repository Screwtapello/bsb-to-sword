redo-ifchange \
    ../script/dilshod/xlsx2csv/xlsx2csv.py \
    ../sources/bsb_tables.xlsx

python3 ../script/dilshod/xlsx2csv/xlsx2csv.py -s 5 -c utf-8 \
    ../sources/bsb_tables.xlsx "$3"
