redo-ifchange \
    ../script/merge-translation-table-data.py \
    bsb-no-footnote-backrefs.osis.xml \
    translations-cleaned.csv

../script/merge-translation-table-data.py -v \
    bsb-no-footnote-backrefs.osis.xml \
    translations-cleaned.csv \
    "$3" > translation-merging-log.txt

# If the output file is unchanged, we don't need to re-run any following tasks.
redo-stamp < "$3"
