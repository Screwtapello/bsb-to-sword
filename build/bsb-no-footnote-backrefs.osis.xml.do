redo-ifchange \
    ../script/Screwtapello/osis-decorators/remove-footnote-backrefs/remove-footnote-backrefs.py \
    bsb-initial.osis.xml

../script/Screwtapello/osis-decorators/remove-footnote-backrefs/remove-footnote-backrefs.py \
    bsb-initial.osis.xml "$3"

# Don't mark this as changed if the content is the same.
redo-stamp < "$3"
