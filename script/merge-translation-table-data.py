#!/usr/bin/env python3
import argparse
import csv
import logging
import re
import sys
import typing
import unittest
from xml.etree import ElementTree as ET

# The XML namespace of OSIS documents
OSIS_NS = "http://www.bibletechnologies.net/2003/OSIS/namespace"

# Namespaced XML tags that are important
DIV_TAG = str(ET.QName(OSIS_NS, "div"))
HEADER_TAG = str(ET.QName(OSIS_NS, "header"))
NOTE_TAG = str(ET.QName(OSIS_NS, "note"))
OSIS_TEXT_TAG = str(ET.QName(OSIS_NS, "osisText"))
REFERENCE_TAG = str(ET.QName(OSIS_NS, "reference"))
TITLE_TAG = str(ET.QName(OSIS_NS, "title"))
TITLE_PAGE_TAG = str(ET.QName(OSIS_NS, "titlePage"))
VERSE_TAG = str(ET.QName(OSIS_NS, "verse"))

WORK_STRONG = "strong"
WORK_BSBLEX = "lemma.BSBlex"

# Values for the "language" field of the CSV file
HEBREW = "Hebrew"
GREEK = "Greek"
ARAMAIC = "Aramaic"

# Convert book names to OSIS references
BOOK_TO_OSIS = {
    "Genesis": "Gen",
    "Exodus": "Exod",
    "Leviticus": "Lev",
    "Numbers": "Num",
    "Deuteronomy": "Deut",
    "Joshua": "Josh",
    "Judges": "Judg",
    "Ruth": "Ruth",
    "1 Samuel": "1Sam",
    "2 Samuel": "2Sam",
    "1 Kings": "1Kgs",
    "2 Kings": "2Kgs",
    "1 Chronicles": "1Chr",
    "2 Chronicles": "2Chr",
    "Ezra": "Ezra",
    "Nehemiah": "Neh",
    "Esther": "Esth",
    "Job": "Job",
    "Psalm": "Ps",
    "Proverbs": "Prov",
    "Ecclesiastes": "Eccl",
    "Song of Solomon": "Song",
    "Isaiah": "Isa",
    "Jeremiah": "Jer",
    "Lamentations": "Lam",
    "Ezekiel": "Ezek",
    "Daniel": "Dan",
    "Hosea": "Hos",
    "Joel": "Joel",
    "Amos": "Amos",
    "Obadiah": "Obad",
    "Jonah": "Jonah",
    "Micah": "Mic",
    "Nahum": "Nah",
    "Habakkuk": "Hab",
    "Zephaniah": "Zeph",
    "Haggai": "Hag",
    "Zechariah": "Zech",
    "Malachi": "Mal",
    "Matthew": "Matt",
    "Mark": "Mark",
    "Luke": "Luke",
    "John": "John",
    "Acts": "Acts",
    "Romans": "Rom",
    "1 Corinthians": "1Cor",
    "2 Corinthians": "2Cor",
    "Galatians": "Gal",
    "Ephesians": "Eph",
    "Philippians": "Phil",
    "Colossians": "Col",
    "1 Thessalonians": "1Thess",
    "2 Thessalonians": "2Thess",
    "1 Timothy": "1Tim",
    "2 Timothy": "2Tim",
    "Titus": "Titus",
    "Philemon": "Phlm",
    "Hebrews": "Heb",
    "James": "Jas",
    "1 Peter": "1Pet",
    "2 Peter": "2Pet",
    "1 John": "1John",
    "2 John": "2John",
    "3 John": "3John",
    "Jude": "Jude",
    "Revelation": "Rev",
}


# Sometimes the TRANSLATION field contains codes
# and ignorable punctuation instead of words.
# To normalise the translation field,
# let's make some less-ambiguous sentinel values.
#
# These constants use only alphanumeric characters
# because we don't want anything to be filtered out.
NOT_TRANSLATED = "NotTranslated"
MERGE_WITH_NEXT = "MergeWithNext"
MERGE_WITH_PREV = "MergeWithPrev"

TRANSLATION_CHAR_RAW_PATTERN = r"[A-Za-z0-9]"
TRANSLATION_CHAR = re.compile(TRANSLATION_CHAR_RAW_PATTERN)
TRANSLATION_WORD = re.compile(TRANSLATION_CHAR_RAW_PATTERN + "+")


def append_text_to_element(
    elem: ET.Element, text: typing.Optional[str]
) -> None:
    """
    Appends text after any content in elem.

    This correctly chooses whether to put the new text in
    the elem's .text property, or the .tail of a child element.
    """
    if len(elem) == 0:
        if elem.text is None:
            elem.text = text
        elif text is not None:
            elem.text += text
    else:
        last_child = elem[-1]
        if last_child.tail is None:
            last_child.tail = text
        elif text is not None:
            last_child.tail += text


class CSVRow(typing.NamedTuple):
    he_ordinal: str
    el_ordinal: str
    en_ordinal: str
    language: str
    verse_ordinal: str
    source_word: str
    transliteration: str
    # This grammar does not appear to be the standard(?) Robinson codes,
    # but a custom scheme:
    #
    # https://biblehub.com/hebrewparse.htm
    # https://biblehub.com/abbrev.htm
    grammar_code: str
    grammar_description: str
    strongs_number: str
    reference: str
    heading: str
    cross_references: str
    translation: str
    footnote: str
    gloss: str


class SourceWord(typing.NamedTuple):
    language: str
    # In Mark 1:1, there's a word with a source "ordinal" of 18379.5.
    # I guess they switched sources and wanted to squeeze in a word
    # without renumbering everything
    source_ordinal: float
    source_word: str
    transliteration: str
    strongs_number: typing.Optional[int]
    grammar_code: str

    @classmethod
    def from_table(cls, row: CSVRow) -> "SourceWord":
        language = row.language
        if language not in (HEBREW, GREEK, ARAMAIC):
            raise RuntimeError(f"Unknown language {language!r}")

        if language == GREEK:
            source_ordinal = float(row.el_ordinal)
        else:  # HEBREW or ARAMAIC
            source_ordinal = float(row.he_ordinal)

        source_word = row.source_word
        assert (
            source_word.strip()
        ), f"Word {row.en_ordinal} is weird: {source_word!r}"
        transliteration = row.transliteration
        if row.strongs_number:
            strongs_number = int(row.strongs_number)
        else:
            strongs_number = None
        grammar_code = row.grammar_code

        return cls(
            language,
            source_ordinal,
            source_word,
            transliteration,
            strongs_number,
            grammar_code,
        )

    def format_osis_attribs(self) -> typing.Dict[str, str]:
        # Some source "words" are actually multiple words,
        # but OSIS wants lemmas to be space-separated,
        # so we need to replace internal spaces in the source word.
        lemmas = [WORK_BSBLEX + ":" + self.source_word.replace(" ", "-")]

        if self.strongs_number is not None:
            if self.language == GREEK:
                prefix = WORK_STRONG + ":G"
            else:  # HEBREW or ARAMAIC
                prefix = WORK_STRONG + ":H"

            lemmas.append(prefix + str(self.strongs_number))

        return {
            "lemma": " ".join(lemmas),
            "xlit": "Latn:" + self.transliteration,
        }


class TargetWord(typing.NamedTuple):
    # In Mark 1:1, there's a word with a target "ordinal" of 323875.5.
    target_ordinal: float
    translation: str

    @classmethod
    def from_table(cls, row: CSVRow) -> "TargetWord":
        target_ordinal = float(row.en_ordinal)
        translation = row.translation.strip()

        return cls(target_ordinal, translation)


class TableEventCrossReference(typing.NamedTuple):
    text: str

    @classmethod
    def from_table(cls, row: CSVRow) -> "TableEventCrossReference":
        return cls(row.cross_references)


class TableEventVerseChange(typing.NamedTuple):
    ordinal: int
    osisID: str

    @classmethod
    def from_table(cls, row: CSVRow) -> "TableEventVerseChange":
        try:
            assert row.verse_ordinal.split()
            book, chapter_and_verse = row.reference.rsplit(" ", 1)
            raw_chapter, raw_verse = chapter_and_verse.split(":")
            assert book in BOOK_TO_OSIS, "Unknownw book " + book
            osis_book = BOOK_TO_OSIS[book]

            return cls(
                int(row.verse_ordinal),
                f"{osis_book}.{raw_chapter}.{raw_verse}",
            )
        except Exception as e:
            e.add_note("Word ordinal " + row.en_ordinal)
            raise e


class TableEventTranslation(typing.NamedTuple):
    source_words: typing.List[SourceWord]
    target_word: typing.Optional[TargetWord]

    def to_osis_element(self) -> ET.Element:
        assert self.source_words
        res = ET.Element(str(ET.QName(OSIS_NS, "w")))
        res.attrib = self.source_words[0].format_osis_attribs()

        for each in self.source_words[1:]:
            for k, v in each.format_osis_attribs().items():
                if k in res.attrib:
                    res.attrib[k] += " " + v
                else:
                    res.attrib[k] = v

        return res


class TableEventFootnote(typing.NamedTuple):
    text: str


TableEvent: typing.TypeAlias = typing.Union[
    TableEventCrossReference,
    TableEventVerseChange,
    TableEventTranslation,
    TableEventFootnote,
]


def iter_table_events(
    table_rows: typing.Iterator[CSVRow],
) -> typing.Iterator[TableEvent]:
    source_words = []

    for row in table_rows:
        if row.cross_references:
            yield TableEventCrossReference.from_table(row)

        if row.reference:
            yield TableEventVerseChange.from_table(row)

        if row.source_word:
            source_words.append(SourceWord.from_table(row))

        if row.translation == MERGE_WITH_NEXT:
            continue

        if row.en_ordinal == "97202":
            # Judges 16:14 starts with added text, then a footnote, then
            # translated text. This is the first instance where the footnote
            # comes *before* the translated text in a row. If this were in the
            # middle of a verse, the added text would be added to the end of
            # the previous row, and the footnote would follow naturally. But
            # because this is the first row of the verse, there *is* no
            # previous row to append the addition to. We can't add it to the
            # tail of the previous verse, because... well, it would be in the
            # wrong verse.
            #
            # We're just going to have to hot-patch the data here.
            # We've already handled the TableEventVerseChange for this row,
            # so let's split off the additional text and the footnote.
            assert row.translation == (
                "[So while he slept  "
                "Delilah took the seven braids of his hair "
                "and wove them into the web]  "
                "Then she tightened [it]"
            ), row
            prefix = (
                "So while he slept, "
                "Delilah took the seven braids of his hair "
                "and wove them into the web"
            )
            for word in prefix.split():
                if not word:
                    continue
                yield TableEventTranslation(
                    source_words=[],
                    target_word=TargetWord(float(row.en_ordinal), word),
                )
            yield TableEventFootnote(row.footnote)

            row = row._replace(
                translation="Then she tightened [it]",
                footnote="",
            )

        prefix = ""
        translation = row.translation
        suffix = ""

        # If there are additonal words inserted
        # at the beginning of this translation,
        # capture them in "prefix"
        if translation.startswith("["):
            prefix, translation = translation[1:].split("]", 1)
            prefix = prefix.strip()
            translation = translation.strip()

        # If there are additional words inserted
        # after this translation,
        # capture them in "suffix"
        if translation.endswith("]"):
            translation, suffix = translation[:-1].rsplit("[", 1)
            suffix = suffix.strip()
            translation = translation.strip()

        # If there are any other inserted words,
        # let's just quietly forget about them.
        translation = translation.replace("[", "")
        translation = translation.replace("]", "")

        if translation == "":
            translation = NOT_TRANSLATED

        if prefix:
            # Yield the prefix words without source words.
            # We yield each word separately,
            # because this string of words
            # might cross XML element boundaries..
            for word in prefix.split():
                if not word:
                    continue
                yield TableEventTranslation(
                    source_words=[],
                    target_word=TargetWord(float(row.en_ordinal), word),
                )

        if source_words or translation != NOT_TRANSLATED:
            yield TableEventTranslation(
                source_words,
                TargetWord(float(row.en_ordinal), translation)
                if translation != NOT_TRANSLATED
                else None,
            )
        source_words.clear()

        if suffix:
            # Yield the additional words without source words.
            # We yield each word separately,
            # because this string of words
            # might cross XML element boundaries..
            for word in suffix.split():
                if not word:
                    continue
                yield TableEventTranslation(
                    source_words=[],
                    target_word=TargetWord(float(row.en_ordinal), word),
                )

        if row.footnote:
            yield TableEventFootnote(row.footnote)


class OSISEventCrossReference(typing.NamedTuple):
    elem: ET.Element


class OSISEventVerseChange(typing.NamedTuple):
    osisID: str


class OSISEventText(typing.NamedTuple):
    text: str


class OSISEventFootnote(typing.NamedTuple):
    elem: ET.Element


OSISEvent: typing.TypeAlias = typing.Union[
    OSISEventCrossReference,
    OSISEventVerseChange,
    OSISEventText,
    OSISEventFootnote,
]


def is_content_canonical(elem: ET.Element, default: bool) -> bool:
    override = elem.attrib.get("canonical")

    if override == "true":
        return True
    elif override == "false":
        return False
    # The following element defaults
    # were extracted from the OSIS schema
    # by searching for the XPath:
    #     //*[@name='canonical'][@default]
    # and looking at the context for each definition.
    elif elem.tag == HEADER_TAG:
        return False
    elif elem.tag == OSIS_TEXT_TAG:
        return True
    elif elem.tag == DIV_TAG:
        return False
    elif elem.tag == NOTE_TAG:
        return False
    elif elem.tag == REFERENCE_TAG:
        return False
    elif elem.tag == TITLE_TAG:
        return False
    elif elem.tag == TITLE_PAGE_TAG:
        return False
    elif elem.tag == VERSE_TAG:
        return True
    else:
        return default


def iter_osis_events(
    elem: ET.Element, context_is_canonical: bool = False
) -> typing.Generator[OSISEvent, typing.Optional[ET.Element], None]:
    if elem.tag == REFERENCE_TAG and elem.attrib.get("type") == "parallel":
        yield OSISEventCrossReference(elem)
        return
    elif elem.tag == NOTE_TAG:
        yield OSISEventFootnote(elem)
        return

    maybe_osisID = elem.attrib.get("osisID", elem.attrib.get("sID"))
    if maybe_osisID is not None and maybe_osisID.count(".") == 2:
        yield OSISEventVerseChange(maybe_osisID)

    wrapper = ET.Element("wrapper")

    content_is_canonical = is_content_canonical(elem, context_is_canonical)

    if content_is_canonical and elem.text:
        # If there's any initial text,
        # yield it so it can be processed.
        replacement = yield OSISEventText(elem.text)
        assert isinstance(replacement, ET.Element)
        wrapper.text = replacement.text
        wrapper.extend(replacement)
    else:
        wrapper.text = elem.text

    for each in elem:
        yield from iter_osis_events(each, content_is_canonical)
        wrapper.append(each)
        if content_is_canonical:
            # Even if there's no following text,
            # pretend there is,
            # so that we can insert additional elements if necessary.
            replacement = yield OSISEventText(each.tail or "")
            assert isinstance(replacement, ET.Element)
            each.tail = replacement.text
            wrapper.extend(replacement)

    # Replace the element's contents
    # with our processed content.
    elem.text = wrapper.text
    elem[:] = wrapper[:]


def translation_to_pattern(text: str) -> str:
    words = TRANSLATION_WORD.findall(text)
    word_break = r"[^A-Za-z0-9]+"

    return r"\b" + word_break.join(words) + r"\b"


def merge_table_events(
    osis_events: typing.Generator[OSISEvent, typing.Optional[ET.Element], None],
    table_events: typing.Iterator[TableEvent],
) -> None:
    current_osis_event: typing.Optional[OSISEvent] = next(osis_events)
    current_table_event: typing.Optional[TableEvent] = next(table_events)

    last_en_ordinal = 0.0
    last_osis_ref = ""

    while current_osis_event != None:
        if isinstance(current_osis_event, OSISEventCrossReference):
            # We put "last_en_ordinal+1" in the messages because cross-reference
            # events happen *before* any translation event for the table row
            # they're part of. Adding 1 should get us back to the row where the
            # error happens, from the last one we saw.
            assert isinstance(current_table_event, TableEventCrossReference), (
                f"In {last_osis_ref} word {last_en_ordinal+1}, "
                f"expected cross reference, not {current_table_event!r}"
            )
            assert current_osis_event.elem.text == current_table_event.text, (
                f"In {last_osis_ref} word {last_en_ordinal+1}, "
                f"expected cross reference {current_osis_event.elem.text!r}, "
                f"not {current_table_event.text!r}"
            )
            current_osis_event = next(osis_events, None)
            current_table_event = next(table_events, None)

        elif isinstance(current_osis_event, OSISEventVerseChange):
            # We put "last_en_ordinal+1" in the messages because verse-change
            # events happen *before* any translation event for the table row
            # they're part of. Adding 1 should get us back to the row where the
            # error happens, from the last one we saw.
            assert isinstance(current_table_event, TableEventVerseChange), (
                f"In {last_osis_ref} word {last_en_ordinal+1}, "
                f"expected verse change, not {current_table_event!r}"
            )
            assert current_osis_event.osisID == current_table_event.osisID, (
                f"In {last_osis_ref} word {last_en_ordinal+1}, "
                f"expected reference {current_osis_event.osisID}, "
                f"not {current_table_event.osisID}"
            )

            old_book = last_osis_ref.split(".", 1)[0]
            new_book = current_osis_event.osisID.split(".", 1)[0]
            if old_book != new_book:
                logging.info("Processing book %r", new_book)

            last_osis_ref = current_osis_event.osisID

            print("-----", current_osis_event.osisID)
            current_osis_event = next(osis_events, None)
            current_table_event = next(table_events, None)

        elif isinstance(current_osis_event, OSISEventText):
            text = current_osis_event.text
            print("OSIS:", text)

            print("Tabl: ", end="")
            # If we have actual translated text,
            # and no source text it's translated from,
            # not even "inserted by the translator" text,
            # something has probably gone weird.
            if TRANSLATION_CHAR.search(text) and not isinstance(
                current_table_event, TableEventTranslation
            ):
                logging.warn(
                    "In %s word %s, missing translation for %r?",
                    last_osis_ref,
                    last_en_ordinal,
                    text,
                )

            wrapper = ET.Element("wrapper")

            while True:
                if not isinstance(current_table_event, TableEventTranslation):
                    break
                if current_table_event.target_word is not None:
                    last_en_ordinal = (
                        current_table_event.target_word.target_ordinal
                    )
                    translation = current_table_event.target_word.translation
                    pattern = translation_to_pattern(translation)
                    m = re.search(pattern, text)
                    if m is None:
                        # We have a word past the end of the current OSIS text,
                        # let's hope the next OSIS event contains it.
                        break
                    else:
                        print(translation, end=" ")

                    if current_table_event.source_words:
                        append_text_to_element(wrapper, text[: m.start()])
                        word_elem = current_table_event.to_osis_element()
                        word_elem.text = text[m.start() : m.end()]
                        wrapper.append(word_elem)
                    else:
                        append_text_to_element(wrapper, text[: m.end()])

                    text = text[m.end() :]
                else:
                    assert current_table_event.source_words
                    wrapper.append(current_table_event.to_osis_element())

                current_table_event = next(table_events, None)

            print()
            append_text_to_element(wrapper, text)
            try:
                current_osis_event = osis_events.send(wrapper)
            except StopIteration:
                break

        elif isinstance(current_osis_event, OSISEventFootnote):
            assert isinstance(current_table_event, TableEventFootnote), (
                f"In {last_osis_ref} word {last_en_ordinal}, "
                f"expected footnote, not {current_table_event!r}"
            )
            print("*****", current_table_event.text)

            try:
                # Quick and dirty conversion from HTML(?) to OSIS markup.
                footnote_markup = (
                    current_table_event.text.replace(
                        "<i>", "<hi type='italic'>"
                    )
                    .replace("</i>", "</hi>")
                    .replace("<span class=|fnv|>", "<hi type='super'>")
                    .replace("</span>", "</hi>")
                )

                # Parse the footnote markup.
                footnote_content = ET.XML(
                    f"<wrapper xmlns='{OSIS_NS}'>{footnote_markup}</wrapper>"
                )

                # Copy the footnote markup into the OSIS document.
                current_osis_event.elem.text = footnote_content.text
                current_osis_event.elem[:] = footnote_content[:]
            except Exception as e:
                e.add_note(f"In {last_osis_ref} word {last_en_ordinal}")
                raise e

            current_osis_event = next(osis_events, None)
            current_table_event = next(table_events, None)

        else:
            raise RuntimeError(
                "Unhandled event type: " + repr(current_osis_event)
            )

    assert current_table_event is None, repr(current_table_event)


def makeOSISWork(name: str, refSystem: str) -> ET.Element:
    res = ET.Element("work", osisWork=name)
    ET.SubElement(res, "refSystem").text = refSystem
    res.tail = "\n"
    return res


def main(raw_args: typing.List[str]) -> int:
    parser = argparse.ArgumentParser(
        description="Merge translation table data into the OSIS document"
    )
    parser.add_argument(
        "--verbose",
        "-v",
        dest="verbose",
        default=0,
        action="count",
        help="Show more details; can be repeated.",
    )
    parser.add_argument(
        "input",
        metavar="INPUT",
        help="The base OSIS file",
        type=argparse.FileType("rb"),
    )
    parser.add_argument(
        "table",
        metavar="TABLE",
        help="The translation table, a UTF-8 encoded CSV file",
        type=argparse.FileType("r", encoding="UTF-8"),
    )
    parser.add_argument(
        "output",
        metavar="OUTPUT",
        help="Write the updated OSIS XML to this file (must not exist yet)",
        type=argparse.FileType("xb"),
    )
    args = parser.parse_args(raw_args[1:])

    logging.basicConfig(
        format="%(levelname)s: %(message)s",
        level=logging.WARNING - (args.verbose * 10),
    )
    logging.captureWarnings(True)

    ET.register_namespace("", OSIS_NS)

    with args.input as input, args.table as table, args.output as output:
        tree = ET.parse(input)

        # Add the work declarations we need for the annotations we're adding.
        header = tree.find("./osis:osisText/osis:header", {"osis": OSIS_NS})
        assert header is not None

        # We'll be adding Strongs lemmas.
        header.append(makeOSISWork(WORK_STRONG, "Dict.Strongs"))

        # We'll be adding our own custom lemmas for the source text.
        header.append(makeOSISWork(WORK_BSBLEX, "Dict.BSBlex"))

        table_lines = iter(table)

        # Skip the copyright line and headers
        next(table_lines)

        # Read each row into a dictionary
        table_rows = (CSVRow(*row) for row in csv.reader(table_lines))

        merge_table_events(
            iter_osis_events(tree.getroot()),
            iter_table_events(table_rows),
        )

        # Write out our modified tree.
        tree.write(output, encoding="utf-8")

    return 0


if __name__ == "__main__":
    sys.exit(main(sys.argv))


class XMLTestCase(unittest.TestCase):
    def assertElementsEqual(
        self,
        got: ET.Element,
        expected: ET.Element,
        msg: typing.Optional[str] = None,
    ) -> None:
        ET.indent(got)
        ET.indent(expected)
        self.assertEqual(
            ET.tostring(got, "unicode"),
            ET.tostring(expected, "unicode"),
            msg,
        )

    def setUp(self) -> None:
        self.addTypeEqualityFunc(ET.Element, self.assertElementsEqual)


class TestAppendTextToElement(XMLTestCase):
    def test_append_text_to_empty_element(self) -> None:
        actual = ET.Element("div")
        append_text_to_element(actual, "hello")

        expected = ET.XML("<div>hello</div>")

        self.assertEqual(actual, expected)

    def test_append_none_to_empty_element(self) -> None:
        actual = ET.Element("div")
        append_text_to_element(actual, None)

        expected = ET.XML("<div/>")

        self.assertEqual(actual, expected)

    def test_append_text_to_wrapper_text(self) -> None:
        actual = ET.XML("<div>hello </div>")
        append_text_to_element(actual, "world")

        expected = ET.XML("<div>hello world</div>")

        self.assertEqual(actual, expected)

    def test_append_none_to_wrapper_text(self) -> None:
        actual = ET.XML("<div>hello</div>")
        append_text_to_element(actual, None)

        expected = ET.XML("<div>hello</div>")

        self.assertEqual(actual, expected)

    def test_append_text_to_tailless_child(self) -> None:
        actual = ET.XML("<div>hello <i>there</i></div>")
        append_text_to_element(actual, " world")

        expected = ET.XML("<div>hello <i>there</i> world</div>")

        self.assertEqual(actual, expected)

    def test_append_none_to_tailless_child(self) -> None:
        actual = ET.XML("<div>hello <i>there</i></div>")
        append_text_to_element(actual, None)

        expected = ET.XML("<div>hello <i>there</i></div>")

        self.assertEqual(actual, expected)

    def test_append_text_to_child_tail(self) -> None:
        actual = ET.XML("<div>hello <i>there</i> </div>")
        append_text_to_element(actual, "world")

        expected = ET.XML("<div>hello <i>there</i> world</div>")

        self.assertEqual(actual, expected)

    def test_append_none_to_child_tail(self) -> None:
        actual = ET.XML("<div>hello <i>there</i> </div>")
        append_text_to_element(actual, None)

        expected = ET.XML("<div>hello <i>there</i> </div>")

        self.assertEqual(actual, expected)
