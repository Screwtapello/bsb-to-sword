[ReferencePatterns]
AbsolutePatterns =
    BOOK FROM_CHAPTER:FROM_VERSE–TO_CHAPTER:TO_VERSE
    BOOK FROM_CHAPTER:FROM_VERSE-TO_CHAPTER:TO_VERSE
    BOOK CHAPTER:FROM_VERSE–TO_VERSE
    BOOK CHAPTER:FROM_VERSE-TO_VERSE
    BOOK CHAPTER:VERSE
    BOOK FROM_CHAPTER–TO_CHAPTER
    BOOK FROM_CHAPTER-TO_CHAPTER
    BOOK CHAPTER

LocationRelativePatterns =
    chapters FROM_CHAPTER–TO_CHAPTER
    chapters FROM_CHAPTER-TO_CHAPTER
    chapters CHAPTER
    chapter CHAPTER
    Verses FROM_VERSE–TO_VERSE
    Verses FROM_VERSE-TO_VERSE
    verses FROM_VERSE–TO_VERSE
    verses FROM_VERSE-TO_VERSE
    verses VERSE
    verse VERSE

ReferenceRelativePatterns =
    Idem CHAPTER:VERSE
    Id CHAPTER:VERSE

ReferenceChainLinks =
    # These links allow chains like "verses 7, 8, 14, 15, 17, and 20"
    , and
    ,
    # This link allows chains like "chapters 18 and 19"
    and
ReferenceChainItems =
    # Used in a footnote in 1Cor.5.12:
    #     Deuteronomy 13:5, 17:7, 19:19, 21:21, 22:21, 22:24, and 24:7
    CHAPTER:VERSE
    # Used in many places, including footnotes for the term "Maskil":
    #     Psalms 32, 42, 44–45, 52–55, 74, 78, 88–89, and 142.
    # ...and verse lists:
    #     see also Ezekiel 20:11, 13, and 21.
    FROM_NUMBER–TO_NUMBER
    FROM_NUMBER-TO_NUMBER
    NUMBER

[BookNames]
# List all the aliases and abbreviations used for each book of scripture
# The "official" abbreviation for each book comes from Appendix C of the
# OSIS 2.1.1 User's Manual.
#
# There can be more than one abbreviation, separated by commas:
#
#     Lam = Lamentations, Lament, Lam, Lm

# Hebrew Bible/Old Testament
Gen = Genesis
Exod = Exodus
Lev = Leviticus
Num = Numbers
Deut = Deuteronomy
Josh = Joshua
Judg = Judges
Ruth = Ruth
1Sam = 1 Samuel
2Sam = 2 Samuel
1Kgs = 1 Kings
2Kgs = 2 Kings
1Chr = 1 Chronicles
2Chr = 2 Chronicles
Ezra = Ezra
Neh = Nehemiah
Esth = Esther
Job = Job
Ps = Psalms, Psalm
Prov = Proverbs
Eccl = Ecclesiastes, Qohelet
Song = Song of Solomon, Song of Songs, Cantlcles, Canticle of Canticles
Isa = Isaiah
Jer = Jeremiah
Lam = Lamentations
Ezek = Ezekiel
Dan = Daniel
Hos = Hosea
Joel = Joel
Amos = Amos
Obad = Obadiah
Jonah = Jonah
Mic = Micah
Nah = Nahum
Hab = Habakkuk
Zeph = Zephaniah
Hag = Haggai
Zech = Zechariah
Mal = Malachi

# New Testament
Matt = Matthew
Mark = Mark
Luke = Luke
John = John
Acts = Acts
Rom = Romans
1Cor = 1 Corinthians
2Cor = 2 Corinthians
Gal = Galatians
Eph = Ephesians
Phil = Philippians
Col = Colossians
1Thess = 1 Thessalonians
2Thess = 2 Thessalonians
1Tim = 1 Timothy
2Tim = 2 Timothy
Titus = Titus
Phlm = Philemon
Heb = Hebrews
Jas = James
1Pet = 1 Peter
2Pet = 2 Peter
1John = 1 John
2John = 2 John
3John = 3 John
Jude = Jude
Rev = Revelation

# Apocrypha and Septuagint
Bar = Baruch
AddDan = Additions to Daniel
PrAzar = Prayer of Azariah, Song of the Three Children
Bel = Bel and the Dragon
SgThree = Song of the Three Young Men
Sus = Susanna
1Esd = 1 Esdras
2Esd = 2 Esdras
AddEsth = Additions to Esther
EpJer = Epistle of Jeremiah
Jdt = Judith
1Macc = 1 Maccabees
2Macc = 2 Maccabees
3Macc = 3 Maccabees
4Macc = 4 Maccabees
PrMan = Prayer of Manasseh
Sir = Sirach, Ecclesiasticus
Tob = Tobit
Wis = Wisdom of Solomon, Wisdom

# All of the following abbreviations come from the CrossWire wiki,
# rather than the OSIS User's Manual:
#
#    http://wiki.crosswire.org/OSIS_Book_Abbreviations
#
# Other Apocrypha/Deuterocanon
EsthGr = Greek Esther
SirP = Sirach Prologue
DanGr = Greek Daniel
AddPs = Psalm 151

# Rahlfs' LXX
Odes = Odes
PssSol = Psalms of Solomon

# Rahlfs' variant books
JoshA = Joshua A
JudgB = Judges B
TobS = Tobit S
SusTh = Susanna θ
DanTh = Daniel θ
BelTh = Bel and the Dragon θ

# Vulgate and other Latin mss.
EpLao = Epistle to the Laodiceans
5Ezra = 5 Ezra
4Ezra = 4 Ezra, Ezra Apocalypse
6Ezra = 6 Ezra
PrSol = Prayer of Solomon
PrJer = Prayer of Jeremiah

# Ethiopian Orthodox Canon
1En = 1 Enoch, Ethiopic Enoch, Ethiopic Apocalypse of Enoch
Jub = Jubilees
4Bar = 4 Baruch, Paraleipomena Jeremiou
1Meq = 1 Meqabyan
2Meq = 2 Meqabyan
3Meq = 3 Meqabyan
Rep = Reproof, Tegsas, Tegsats, Taagsas
AddJer = Additions to Jeremiah, Rest of Jeremiah
PsJos = Pseudo-Josephus, Jossipon, Joseph ben Gorion's Medieval History of the Jews

# Armenian Orthodox Canon Additions
EpCorPaul = Epistle of the Corinthians to Paul
3Cor = 3 Corinthians
WSir = Words of Sirach
PrEuth = Prayer of Euthalius
DormJohn = Dormition of John
JosAsen = Joseph and Asenath
T12Patr = Testaments of the Twelve Patriarchs
T12Patr.TAsh = Testament of Asher
T12Patr.TBenj = Testament of Benjamin
T12Patr.TDan = Testament of Dan
T12Patr.TGad = Testament of Gad
T12Patr.TIss = Testament of Issachar
T12Patr.TJos = Testament of Joseph
T12Patr.TJud = Testament of Judah
T12Patr.TLevi = Testament of Levi
T12Patr.TNaph = Testament of Naphtali
T12Patr.TReu = Testament of Reuben
T12Patr.TSim = Testament of Simeon
T12Patr.TZeb = Testament of Zebulun

# Peshitta/Syriac Orthodox Canon
2Bar = 2 Baruch, Apocalypse of Baruch, Syriac Apocalypse of Baruch
EpBar = Letter of Baruch
5ApocSyrPss = Additional Syriac Psalms, 5 Apocryphal Syriac Psalms
JosephusJWvi = Josephus' Jewish War VI

# Apostolic Fathers
1Clem = 1 Clement
2Clem = 2 Clement
IgnEph = Ignatius to the Ephesians
IgnMagn = Ignatius to the Magnesians
IgnTrall = Ignatius to the Trallians
IgnRom = Ignatius to the Romans
IgnPhld = Ignatius to the Philadelphians
IgnSmyrn = Ignatius to the Smyrnaeans
IgnPol = Ignatius to Polycarp
PolPhil = Polycarp to the Philippians
MartPol = Martyrdom of Polycarp
Did = Didache
Barn = Barnabas
Herm = Shepherd of Hermas
Herm.Mand = Shepherd of Hermas, Mandates
Herm.Sim = Shepherd of Hermas, Similitudes
Herm.Vis = Shepherd of Hermas, Visions
Diogn = Diognetus
AposCreed = Apostles' Creed
PapFrag = Fragments of Papias
RelElders = Reliques of the Elders
QuadFrag = Fragment of Quadratus

# Things that aren't Scripture
NotABook = Jasher
