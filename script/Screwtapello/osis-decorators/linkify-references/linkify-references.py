#!/usr/bin/env python3
import argparse
import configparser
import logging
import re
import sys
import typing
import unittest
from xml.etree import ElementTree as ET

# The XML namespace of OSIS documents
OSIS_NS = "http://www.bibletechnologies.net/2003/OSIS/namespace"
# The official OSIS abbreviations for book names.
OSIS_ABBREVIATIONS = """
    Gen Exod Lev Num Deut
    Josh Judg Ruth 1Sam 2Sam 1Kgs 2Kgs 1Chr 2Chr Ezra Neh Esth Job
    Ps Prov Eccl Song
    Isa Jer Lam Ezek Dan
    Hos Joel Amos Obad Jonah Mic Nah Hab Zeph Hag Zech Mal

    Matt Mark Luke John
    Acts
    Rom 1Cor 2Cor Gal Eph Phil Col 1Thess 2Thess 1Tim 2Tim Titus Phlm Heb Jas
    1Pet 2Pet 1John 2John 3John Jude Rev

    Bar AddDan PrAzar Bel SgThree Sus 1Esd 2Esd AddEsth EpJer Jdt
    1Macc 2Macc 3Macc 4Macc PrMan Sir Tob Wis

    EsthGr SirP DanGr AddPs

    Odes PssSol

    JoshA JudgB TobS SusTh DanTh BelTh

    EpLao 5Ezra 4Ezra 6Ezra PrSol PrJer

    1En Jub 4Bar 1Meq 2Meq 3Meq Rep AddJer PsJos

    EpCorPaul 3Cor WSir PrEuth DormJohn JosAsen
    T12Patr T12Patr.TAsh
    T12Patr.TBenj T12Patr.TDan T12Patr.TGad T12Patr.TIss T12Patr.TJos
    T12Patr.TJud T12Patr.TLevi T12Patr.TNaph T12Patr.TReu T12Patr.TSim
    T12Patr.TZeb

    2Bar EpBar 5ApocSyrPss JosephusJWvi

    1Clem 2Clem IgnEph IgnMagn IgnTrall IgnRom IgnPhld IgnSmyrn IgnPol PolPhil
    MartPol Did Barn Herm Herm.Mand Herm.Sim Herm.Vis Diogn AposCreed PapFrag
    RelElders QuadFrag
    """.split()

# The special tokens used in our reference patterns.
BOOK = "BOOK"
CHAPTER = "CHAPTER"
FROM_CHAPTER = "FROM_CHAPTER"
TO_CHAPTER = "TO_CHAPTER"
VERSE = "VERSE"
FROM_VERSE = "FROM_VERSE"
TO_VERSE = "TO_VERSE"
NUMBER = "NUMBER"
FROM_NUMBER = "FROM_NUMBER"
TO_NUMBER = "TO_NUMBER"

CHAPTER_TOKENS = {CHAPTER, FROM_CHAPTER, TO_CHAPTER}
VERSE_TOKENS = {VERSE, FROM_VERSE, TO_VERSE}

# Some times only one of a kind of token can appear in a pattern.
MUTUALLY_EXCLUSIVE_TOKENS = [
    {CHAPTER, FROM_CHAPTER, NUMBER},
    {CHAPTER, TO_CHAPTER, NUMBER},
    {VERSE, FROM_CHAPTER, NUMBER},
    {VERSE, TO_CHAPTER, NUMBER},
    {VERSE, FROM_VERSE, NUMBER},
    {VERSE, TO_VERSE, NUMBER},
    {NUMBER, FROM_NUMBER},
    {NUMBER, TO_NUMBER},
]

# Sometimes if you have one kind of token, you need others.
MUTUALLY_REQUIRED_TOKENS = [
    {FROM_CHAPTER, TO_CHAPTER},
    {FROM_VERSE, TO_VERSE},
    {FROM_NUMBER, TO_NUMBER},
]

# If a text fragment matches this regex,
# we may have miss-parsed a reference.
SUSPICIOUS_FRAGMENT_RE = re.compile(r"[-:\u2013]\d")

# Identifiers used in the configuration file
REFERENCE_PATTERNS = "ReferencePatterns"
BOOK_NAMES = "BookNames"
ABSOLUTE_PATTERNS = "AbsolutePatterns"
LOCATION_RELATIVE_PATTERNS = "LocationRelativePatterns"
REFERENCE_RELATIVE_PATTERNS = "ReferenceRelativePatterns"
REFERENCE_CHAIN_LINKS = "ReferenceChainLinks"
REFERENCE_CHAIN_ITEMS = "ReferenceChainItems"
NOT_A_BOOK = "NotABook"


def append_text_to_element(
    elem: ET.Element, text: typing.Optional[str]
) -> None:
    """
    Appends text after any content in elem.

    This correctly chooses whether to put the new text in
    the elem's .text property, or the .tail of a child element.
    """
    if len(elem) == 0:
        if elem.text is None:
            elem.text = text
        elif text is not None:
            elem.text += text
    else:
        last_child = elem[-1]
        if last_child.tail is None:
            last_child.tail = text
        elif text is not None:
            last_child.tail += text


class ReusedToken(Exception):
    pattern: str
    token: str

    def __init__(self, pattern: str, token: str) -> None:
        self.pattern = pattern
        self.token = token
        super().__init__(pattern, token)

    def __str__(self) -> str:
        return f"Token {self.token!r} used twice in pattern: {self.pattern!r}"


class IncompatibleTokens(Exception):
    pattern: str
    token1: str
    token2: str

    def __init__(self, pattern: str, token1: str, token2: str) -> None:
        self.pattern = pattern
        self.token1 = token1
        self.token2 = token2
        super().__init__(pattern, token1, token2)

    def __str__(self) -> str:
        return (
            f"Cannot use {self.token1!r} and {self.token2!r} "
            f"in the same pattern: {self.pattern!r}"
        )


class MissingToken(Exception):
    pattern: str
    required_token: str
    requiring_token: typing.Optional[str]

    def __init__(
        self,
        pattern: str,
        required_token: str,
        requiring_token: typing.Optional[str] = None,
    ) -> None:
        self.pattern = pattern
        self.required_token = required_token
        self.requiring_token = requiring_token
        super().__init__(pattern, required_token, requiring_token)

    def __str__(self) -> str:
        parts = [f"Missing {self.required_token!r} "]

        if self.requiring_token is not None:
            parts.append(f"to go with {self.requiring_token!r} ")

        parts.append(f"in pattern: {self.pattern!r}")

        return "".join(parts)


class UnwantedToken(Exception):
    pattern: str
    token: str

    def __init__(self, pattern: str, token: str) -> None:
        self.pattern = pattern
        self.token = token
        super().__init__(pattern, token)

    def __str__(self) -> str:
        return f"Token {self.token!r} should not be in pattern {self.pattern!r}"


class PatternCompiler:
    token_patterns: typing.Dict[str, str]
    token_re: re.Pattern[str]

    def __init__(self, book_names: typing.Iterable[str]) -> None:
        # Make a regex that matches any book name that we can map to an OSIS
        # book ID.
        books_re = "|".join(re.escape(name) for name in book_names)

        # No chapter or verse zero.
        # Also, don't accept the "1" in "1,000" or "1.5".
        number_re = r"[1-9]\d*\b(?![.,]\d)"

        # Map the tokens in our reference patterns to regexes.
        self.token_patterns = {
            BOOK: books_re,
            CHAPTER: number_re,
            FROM_CHAPTER: number_re,
            TO_CHAPTER: number_re,
            VERSE: number_re,
            FROM_VERSE: number_re,
            TO_VERSE: number_re,
            NUMBER: number_re,
            FROM_NUMBER: number_re,
            TO_NUMBER: number_re,
        }
        # Make a regex that matches the tokens in our our reference patterns.
        self.token_re = re.compile(
            "|".join(re.escape(name) for name in self.token_patterns.keys())
            + "|."
        )

    def compile_pattern(self, kind: str, index: int, raw_pattern: str) -> str:
        pattern = []
        seen_tokens = set()
        for token in self.token_re.findall(raw_pattern):
            if kind != ABSOLUTE_PATTERNS and token == BOOK:
                raise UnwantedToken(raw_pattern, token)
            if token in self.token_patterns:
                if token in seen_tokens:
                    raise ReusedToken(raw_pattern, token)
                seen_tokens.add(token)

                pattern.extend(
                    [
                        "(?P<",
                        kind,
                        "_",
                        str(index),
                        "_",
                        token,
                        ">",
                        self.token_patterns[token],
                        ")",
                    ]
                )
            else:
                pattern.append(re.escape(token))

        if len(seen_tokens) == 0:
            raise ValueError(f"Pattern contains no tokens: {pattern!r}")

        for token_set in MUTUALLY_EXCLUSIVE_TOKENS:
            dangerous_tokens = sorted(token_set.intersection(seen_tokens))
            if len(dangerous_tokens) > 1:
                raise IncompatibleTokens(
                    raw_pattern, dangerous_tokens[0], dangerous_tokens[1]
                )

        for token_set in MUTUALLY_REQUIRED_TOKENS:
            present_tokens = sorted(token_set.intersection(seen_tokens))
            missing_tokens = sorted(token_set - seen_tokens)
            if len(present_tokens) != 0 and len(missing_tokens) != 0:
                raise MissingToken(
                    raw_pattern, missing_tokens[0], present_tokens[0]
                )

        if kind == ABSOLUTE_PATTERNS:
            if BOOK not in seen_tokens:
                raise MissingToken(raw_pattern, BOOK)

            # An absolute pattern must be BOOK, BOOK CHAPTER, or
            # BOOK CHAPTER VERSE, not BOOK VERSE.
            if seen_tokens.intersection(
                VERSE_TOKENS
            ) and seen_tokens.isdisjoint(CHAPTER_TOKENS):
                raise MissingToken(raw_pattern, CHAPTER)

        return "".join(pattern)


def compile_reference_re(
    compiler: PatternCompiler,
    absolute_patterns: typing.Iterable[str],
    location_relative_patterns: typing.Iterable[str],
    reference_relative_patterns: typing.Iterable[str],
) -> re.Pattern:
    # Convert our reference patterns into Python regexes.
    patterns: typing.List[str] = []
    patterns.extend(
        compiler.compile_pattern(ABSOLUTE_PATTERNS, index, each)
        for (index, each) in enumerate(absolute_patterns)
    )
    patterns.extend(
        compiler.compile_pattern(LOCATION_RELATIVE_PATTERNS, index, each)
        for (index, each) in enumerate(location_relative_patterns)
    )
    patterns.extend(
        compiler.compile_pattern(REFERENCE_RELATIVE_PATTERNS, index, each)
        for (index, each) in enumerate(reference_relative_patterns)
    )

    if len(patterns) == 0:
        # We don't have any patterns we want to match,
        # so let's make sure we produce a pattern that doesn't match anything
        # rather than a pattern that zero-width matches everything.
        patterns = ["[^\U00000000-\U0010FFFF]"]

    return re.compile("|".join(patterns))


def compile_reference_chain_re(
    compiler: PatternCompiler,
    reference_chain_links: typing.Iterable[str],
    reference_chain_items: typing.Iterable[str],
) -> typing.Tuple[re.Pattern, re.Pattern]:
    link_patterns = [re.escape(each) for each in reference_chain_links]

    if len(link_patterns) == 0:
        # We don't have any patterns we want to match,
        # so let's make sure we produce a pattern that doesn't match anything
        # rather than a pattern that zero-width matches everything.
        link_patterns = ["[^\U00000000-\U0010FFFF]"]

    links_re = re.compile(r"\s*(" + "|".join(link_patterns) + r")\s*")

    item_patterns = [
        compiler.compile_pattern(REFERENCE_CHAIN_ITEMS, index, each)
        for (index, each) in enumerate(reference_chain_items)
    ]

    if len(item_patterns) == 0:
        # We don't have any patterns we want to match,
        # so let's make sure we produce a pattern that doesn't match anything
        # rather than a pattern that zero-width matches everything.
        item_patterns = ["[^\U00000000-\U0010FFFF]"]

    items_re = re.compile("|".join(item_patterns))

    return links_re, items_re


class Configuration:
    book_name_to_osis: typing.Dict[str, typing.Optional[str]]
    absolute_patterns: typing.List[str]
    location_relative_patterns: typing.List[str]
    reference_relative_patterns: typing.List[str]
    reference_chain_links: typing.List[str]
    reference_chain_items: typing.List[str]

    def __init__(
        self,
        book_name_to_osis: typing.Dict[str, typing.Optional[str]],
        absolute_patterns: typing.List[str],
        location_relative_patterns: typing.List[str],
        reference_relative_patterns: typing.List[str],
        reference_chain_links: typing.List[str],
        reference_chain_items: typing.List[str],
    ) -> None:
        self.book_name_to_osis = book_name_to_osis
        self.absolute_patterns = absolute_patterns
        self.location_relative_patterns = location_relative_patterns
        self.reference_relative_patterns = reference_relative_patterns
        self.reference_chain_links = reference_chain_links
        self.reference_chain_items = reference_chain_items

    @classmethod
    def from_file(cls, source: typing.TextIO) -> "Configuration":
        config = configparser.ConfigParser(comment_prefixes=("#",))
        config.add_section(REFERENCE_PATTERNS)
        config.add_section(BOOK_NAMES)
        config.read_file(source)

        # Map book names in references back to the canonical OSIS book ID.
        book_name_to_osis: typing.Dict[str, typing.Optional[str]] = {}
        for osis_name in OSIS_ABBREVIATIONS:
            raw_names = config[BOOK_NAMES].get(osis_name, "")
            for name in [name.strip() for name in raw_names.split(",")]:
                if name:
                    book_name_to_osis[name] = osis_name

        # Map things that look like books but aren't, to None.
        non_books = config[BOOK_NAMES].get(NOT_A_BOOK, "")
        for name in [name.strip() for name in non_books.split(",")]:
            if name:
                book_name_to_osis[name] = None

        def split_patterns(raw_value: str) -> typing.List[str]:
            # Split into lines
            res = [line.strip() for line in raw_value.split("\n")]
            # Remove blank lines.
            res = [line for line in res if line]
            return res

        return cls(
            book_name_to_osis,
            split_patterns(
                config[REFERENCE_PATTERNS].get(ABSOLUTE_PATTERNS, "")
            ),
            split_patterns(
                config[REFERENCE_PATTERNS].get(LOCATION_RELATIVE_PATTERNS, "")
            ),
            split_patterns(
                config[REFERENCE_PATTERNS].get(REFERENCE_RELATIVE_PATTERNS, "")
            ),
            split_patterns(
                config[REFERENCE_PATTERNS].get(REFERENCE_CHAIN_LINKS, "")
            ),
            split_patterns(
                config[REFERENCE_PATTERNS].get(REFERENCE_CHAIN_ITEMS, "")
            ),
        )


class OsisId(typing.NamedTuple):
    book: str
    chapter: typing.Optional[int]
    verse: typing.Optional[int]

    def __str__(self) -> str:
        if self.verse is not None:
            return f"{self.book}.{self.chapter}.{self.verse}"
        if self.chapter is not None:
            return f"{self.book}.{self.chapter}"
        return self.book

    @classmethod
    def from_raw(cls, raw_id: str) -> "OsisId":
        parts = raw_id.split(".")
        if len(parts) > 3:
            raise ValueError(f"osisID must be book.chapter.verse: {raw_id!r}")

        book = parts[0]
        if book not in OSIS_ABBREVIATIONS:
            raise ValueError(f"Unrecognised OSIS book ID {book!r}: {raw_id!r}")

        chapter = None
        if len(parts) > 1:
            try:
                chapter = int(parts[1])
            except ValueError as e:
                e.add_note(
                    f"Parsing chapter {parts[1]!r} from osisID {raw_id!r}"
                )
                raise e

            if chapter <= 0:
                raise ValueError(
                    f"Chapter must be natural number, not {chapter!r} in osisID {raw_id!r}"
                )

        verse = None
        if len(parts) > 2:
            try:
                verse = int(parts[2])
            except ValueError as e:
                e.add_note(f"Parsing verse {parts[2]!r} from osisID {raw_id!r}")
                raise e

            if verse <= 0:
                raise ValueError(
                    f"Verse must be natural number, not {verse!r} in osisID {raw_id!r}"
                )

        return cls(book, chapter, verse)

    def merge_parts(
        self,
        new_book: typing.Optional[str],
        new_chapter: typing.Optional[int],
        new_verse: typing.Optional[int],
    ) -> "OsisId":
        if new_book is not None:
            return OsisId(new_book, new_chapter, new_verse)
        if new_chapter is not None:
            return OsisId(self.book, new_chapter, new_verse)
        if new_verse is not None:
            return OsisId(self.book, self.chapter, new_verse)

        return self


class ReferenceContext:
    last_location: typing.Optional[OsisId]
    last_reference: typing.Optional[OsisId]

    def __init__(
        self,
        last_location: typing.Optional[OsisId] = None,
        last_reference: typing.Optional[OsisId] = None,
    ) -> None:
        self.last_location = last_location
        self.last_reference = last_reference

    def saw_location(self, new_location: OsisId) -> None:
        self.last_location = new_location
        # In a new location, don't let old references confuse a stray reference-
        # relative reference.
        self.last_reference = None

    def saw_reference(self, new_reference: OsisId) -> None:
        self.last_reference = new_reference


def reference_match_to_osis_ref(
    book_name_to_osis: typing.Mapping[str, typing.Optional[str]],
    context: ReferenceContext,
    ref_match: re.Match[str],
) -> typing.Optional[str]:
    """
    Convert a match of the reference regex to an OSIS ID reference.
    """
    match_groups = ref_match.groupdict()
    match_book = None
    match_chapter = None
    match_from_chapter = None
    match_to_chapter = None
    match_verse = None
    match_from_verse = None
    match_to_verse = None
    pattern_kind = None
    pattern_index = None

    if ref_match.group() == "":
        logging.warning(
            "In %s, got a zero-length match for pattern %r, ignoring",
            context.last_location,
            ref_match.re.pattern,
        )
        return None

    for group_name, group_text in ref_match.groupdict().items():
        if group_text is None:
            continue

        pattern_kind, pattern_index, field_name = group_name.split("_", 2)
        if field_name == BOOK:
            match_book = book_name_to_osis[group_text]
            if match_book is None:
                logging.info(
                    "In %s, %r is not a book, ignoring reference %r",
                    context.last_location,
                    group_text,
                    ref_match.group(0),
                )
                return None
        elif field_name == CHAPTER:
            match_chapter = int(group_text)
        elif field_name == FROM_CHAPTER:
            match_from_chapter = int(group_text)
        elif field_name == TO_CHAPTER:
            match_to_chapter = int(group_text)
        elif field_name == VERSE:
            match_verse = int(group_text)
        elif field_name == FROM_VERSE:
            match_from_verse = int(group_text)
        elif field_name == TO_VERSE:
            match_to_verse = int(group_text)
        elif field_name == NUMBER:
            if context.last_reference is not None:
                if context.last_reference.verse is not None:
                    match_verse = int(group_text)
                else:
                    match_chapter = int(group_text)
        elif field_name == FROM_NUMBER:
            if context.last_reference is not None:
                if context.last_reference.verse is not None:
                    match_from_verse = int(group_text)
                else:
                    match_from_chapter = int(group_text)
        elif field_name == TO_NUMBER:
            if context.last_reference is not None:
                if context.last_reference.verse is not None:
                    match_to_verse = int(group_text)
                else:
                    match_to_chapter = int(group_text)
        else:
            raise Exception(f"Got unexpected match group {group_name!r}")

    if match_from_chapter is not None and match_to_chapter is not None:
        # If we're pointing to a chapter range,
        # but the chapter numbers are the wrong way around,
        # that's a problem.
        if match_from_chapter > match_to_chapter:
            logging.warning(
                "In %s, reference %r has chapter numbers in the wrong order",
                context.last_location,
                ref_match.group(0),
            )
            return None

        # If we're pointing to a chapter range,
        # but both ends are in the same chapter...
        if match_from_chapter == match_to_chapter:
            # ...if the range includes verses...
            if match_from_verse is not None and match_to_verse is not None:
                # ...and the verses are *not* in the right order,
                # that's a problem.
                if match_from_verse > match_to_verse:
                    logging.warning(
                        "In %s, reference %r has verse numbers in the wrong order",
                        context.last_location,
                        ref_match.group(0),
                    )
                    return None

                if match_from_verse == match_to_verse:
                    logging.warning(
                        "In %s, reference %r has identical verse numbers",
                        context.last_location,
                        ref_match.group(0),
                    )
                    return None

            # If the range does *not* include verses,
            # then we have a redundant range,
            # and that's a problem.
            else:
                logging.warning(
                    "In %s, reference %r has identical chapter numbers",
                    context.last_location,
                    ref_match.group(0),
                )
                return None

    if match_from_verse is not None and match_to_verse is not None:
        # If we're not pointing to a chapter range,
        # but the verse numbers are the wrong way around,
        # that's a problem
        if match_from_chapter is None:
            if match_from_verse > match_to_verse:
                logging.warning(
                    "In %s, reference %r has verse numbers in the wrong order",
                    context.last_location,
                    ref_match.group(0),
                )
                return None

            if match_from_verse == match_to_verse:
                logging.warning(
                    "In %s, reference %r has identical verse numbers",
                    context.last_location,
                    ref_match.group(0),
                )
                return None

    if match_from_chapter is None:
        match_from_chapter = match_chapter
    if match_to_chapter is None:
        match_to_chapter = match_chapter
    if match_from_verse is None:
        match_from_verse = match_verse
    if match_to_verse is None:
        match_to_verse = match_verse

    if pattern_kind == ABSOLUTE_PATTERNS:
        assert match_book is not None
        from_id = OsisId(match_book, match_from_chapter, match_from_verse)
        to_id = OsisId(match_book, match_to_chapter, match_to_verse)

    elif pattern_kind == LOCATION_RELATIVE_PATTERNS:
        if context.last_location is None:
            logging.warning(
                "Got location-relative reference %r before first location?",
                ref_match.group(0),
            )
            return None

        from_id = context.last_location.merge_parts(
            None, match_from_chapter, match_from_verse
        )
        to_id = context.last_location.merge_parts(
            None, match_to_chapter, match_to_verse
        )

    elif pattern_kind in (REFERENCE_RELATIVE_PATTERNS, REFERENCE_CHAIN_ITEMS):
        if context.last_reference is None:
            logging.warning(
                "In %s, reference-relative reference %r "
                "occurs before first reference?",
                context.last_location,
                ref_match.group(0),
            )
            return None

        from_id = context.last_reference.merge_parts(
            None, match_from_chapter, match_from_verse
        )
        to_id = context.last_reference.merge_parts(
            None, match_to_chapter, match_to_verse
        )

    else:
        logging.warning(
            "In %s, got match with unexpected kind %r",
            context.last_location,
            pattern_kind,
        )
        return None

    context.saw_reference(to_id)

    if from_id == to_id:
        return str(from_id)
    else:
        return f"{from_id}-{to_id}"


def iter_reference_matches_in_text(
    text: str,
    reference_re: re.Pattern,
) -> typing.Iterator[typing.Union[str, re.Match]]:
    """
    Yield matches for reference_re, and the text spans between them.
    """
    pos = 0

    # For all the matches in this text...
    for m in reference_re.finditer(text, pos):
        # If we skipped over any text to find the next match...
        if pos != m.start():
            # ...then yield that text.
            yield text[pos : m.start()]
        # Yield the match too.
        yield m
        # Keep track of how much of the text we've searched.
        pos = m.end()

    # Yield any remaining text after the last match.
    if pos != len(text):
        yield text[pos:]


def add_reference_chain_matches(
    reference_matches: typing.Iterator[typing.Union[str, re.Match]],
    reference_chain_links_re: re.Pattern,
    reference_chain_items_re: re.Pattern,
) -> typing.Iterator[typing.Union[str, re.Match]]:
    """
    Add reference chains to an iterator of reference matches and text.
    """
    # Have we seen a reference match in the current text span?
    seen_match = False
    for text_or_match in reference_matches:
        # If this item is an existing reference match,
        # pass it along.
        if isinstance(text_or_match, re.Match):
            seen_match = True
            yield text_or_match
            continue

        if not seen_match:
            # We can't have a reference chain
            # until we've had a reference.
            yield text_or_match
            continue

        # We got some text!
        text = text_or_match
        pos = 0

        # A reference chain is one-or-more instances of a (link, item) pair.
        while True:
            # If this text span doesn't start with a link match,
            # it can't be a chain.
            link_match = reference_chain_links_re.match(text, pos)
            if link_match is None:
                break

            # We got a link match!
            # It's not a reference so we don't care about the match object,
            # we'll just yield the text.
            yield text[pos : link_match.end()]
            pos = link_match.end()

            # If the link isn't immediately followed by a link item,
            # then it's not a reference chain.
            item_match = reference_chain_items_re.match(text, pos)
            if item_match is None:
                break

            # We got a link item!
            # Yield the item, and in the next loop we'll check for another link.
            yield item_match
            pos = item_match.end()

        # Yield any more text after end of the reference chain.
        if pos != len(text):
            yield text[pos:]


def collect_reference_matches(
    reference_matches: typing.Iterator[typing.Union[str, re.Match]],
    book_name_to_osis: typing.Mapping[str, typing.Optional[str]],
    context: ReferenceContext,
) -> ET.Element:
    res = ET.Element(str(ET.QName(OSIS_NS, "wrapper")))

    for text_or_match in reference_matches:
        # If this is a reference match....
        if isinstance(text_or_match, re.Match):
            # ...try to decode it.
            osisRef = reference_match_to_osis_ref(
                book_name_to_osis, context, text_or_match
            )
            # If we decode it successfully...
            if osisRef is not None:
                # ...record it as an OSIS reference element...
                ET.SubElement(
                    res,
                    str(ET.QName(OSIS_NS, "reference")),
                    type="source",
                    osisRef=osisRef,
                ).text = text_or_match.group()

                # ..and move along to the next text_or_match.
                continue

            # This isn't actually a reference,
            # so let's treat it as text.
            # reference_match_to_osis_ref() should have already
            # logged details of the problem.
            text_or_match = text_or_match.group()

        # text_or_match was always text, or we've converted it to text.
        assert isinstance(text_or_match, str)
        append_text_to_element(res, text_or_match)

    return res


def add_references_to_text(
    text: str,
    reference_re: re.Pattern,
    reference_chain_links_re: re.Pattern,
    reference_chain_items_re: re.Pattern,
    book_name_to_osis: typing.Mapping[str, typing.Optional[str]],
    context: ReferenceContext,
) -> ET.Element:
    """
    Scans text, looking for references it can turn into reference elements.
    """
    matches = iter_reference_matches_in_text(text, reference_re)
    matches = add_reference_chain_matches(
        matches, reference_chain_links_re, reference_chain_items_re
    )
    res = collect_reference_matches(matches, book_name_to_osis, context)

    if res.text is not None:
        for m in SUSPICIOUS_FRAGMENT_RE.finditer(res.text):
            logging.warning(
                "In %s, found suspicious fragment %r",
                context.last_location,
                m.group(),
            )

    for child in res:
        if child.tail is None:
            continue
        for m in SUSPICIOUS_FRAGMENT_RE.finditer(child.tail):
            logging.warning(
                "In %s, found suspicious fragment %r",
                context.last_location,
                m.group(),
            )

    return res


def _add_references_to_element(
    elem: ET.Element,
    reference_re: re.Pattern,
    reference_chain_links_re: re.Pattern,
    reference_chain_items_re: re.Pattern,
    book_name_to_osis: typing.Mapping[str, typing.Optional[str]],
    context: ReferenceContext,
) -> None:
    new_children = ET.Element("wrapper")

    if "osisID" in elem.attrib:
        ids = elem.attrib["osisID"].split()
        if len(ids):
            context.saw_location(OsisId.from_raw(ids[0]))

    # Look for references in the text of this element
    if elem.text is not None:
        wrapper = add_references_to_text(
            elem.text,
            reference_re,
            reference_chain_links_re,
            reference_chain_items_re,
            book_name_to_osis,
            context,
        )
        append_text_to_element(new_children, wrapper.text)
        new_children.extend(wrapper[:])

    for child in elem:
        if child.tag == str(ET.QName(OSIS_NS, "reference")):
            if "osisRef" not in child.attrib:
                # If this is a reference without a target...
                ref_type = child.attrib.get("type", "source")
                _add_references_to_element(
                    child,
                    reference_re,
                    reference_chain_links_re,
                    reference_chain_items_re,
                    book_name_to_osis,
                    context,
                )
                # copy the type attribute to any references inside it
                for inner_ref in child.iterfind(
                    ".//osis:reference", {"osis": OSIS_NS}
                ):
                    inner_ref.attrib["type"] = ref_type

                # replace this element with its children
                append_text_to_element(new_children, child.text)
                new_children.extend(child[:])

            else:
                # This is a reference with a target, leave it be.
                new_children.append(child)

        else:
            _add_references_to_element(
                child,
                reference_re,
                reference_chain_links_re,
                reference_chain_items_re,
                book_name_to_osis,
                context,
            )
            new_children.append(child)

        # Look for references in the tail of this child.
        if child.tail is not None:
            wrapper = add_references_to_text(
                child.tail,
                reference_re,
                reference_chain_links_re,
                reference_chain_items_re,
                book_name_to_osis,
                context,
            )
            # At this point, child might or might not be in new_children,
            # depending on whether it's a broken <reference/> element
            # we want to drop.
            # We can't blindly put wrapper.text into child.tail
            # (or it might be lost)
            # or blindly append it to new_children
            # (or it might be doubled).
            child.tail = None
            append_text_to_element(new_children, wrapper.text)
            new_children.extend(wrapper[:])

    elem.text = new_children.text
    elem[:] = new_children


def add_references(
    root: ET.Element,
    reference_re: re.Pattern,
    reference_chain_links_re: re.Pattern,
    reference_chain_items_re: re.Pattern,
    book_name_to_osis: typing.Mapping[str, typing.Optional[str]],
) -> None:
    _add_references_to_element(
        root,
        reference_re,
        reference_chain_links_re,
        reference_chain_items_re,
        book_name_to_osis,
        ReferenceContext(),
    )


def main(raw_args: typing.List[str]) -> int:
    parser = argparse.ArgumentParser(
        description="Annotate cross-references in an OSIS document"
    )
    parser.add_argument(
        "--verbose",
        "-v",
        dest="verbose",
        default=0,
        action="count",
        help="Show more details; can be repeated.",
    )
    parser.add_argument(
        "input",
        metavar="INPUT",
        help="Scan this OSIS XML file for text that looks like references",
        type=argparse.FileType("rb"),
    )
    parser.add_argument(
        "config",
        metavar="CONFIG",
        help="Configuration file describing how to recognise references",
        type=argparse.FileType("r"),
    )
    parser.add_argument(
        "output",
        metavar="OUTPUT",
        help="Write the updated OSIS XML to this file (must not exist yet)",
        type=argparse.FileType("xb"),
    )
    args = parser.parse_args(raw_args[1:])

    logging.basicConfig(
        format="%(levelname)s: %(message)s",
        level=logging.WARNING - (args.verbose * 10),
    )
    logging.captureWarnings(True)

    ET.register_namespace("", OSIS_NS)

    with args.input as input, args.config as config, args.output as output:
        tree = ET.parse(input)

        config = Configuration.from_file(config)

        compiler = PatternCompiler(config.book_name_to_osis.keys())
        reference_re = compile_reference_re(
            compiler,
            config.absolute_patterns,
            config.location_relative_patterns,
            config.reference_relative_patterns,
        )
        (
            reference_chain_links_re,
            reference_chain_items_re,
        ) = compile_reference_chain_re(
            compiler, config.reference_chain_links, config.reference_chain_items
        )

        add_references(
            tree.getroot(),
            reference_re,
            reference_chain_links_re,
            reference_chain_items_re,
            config.book_name_to_osis,
        )

        tree.write(output, encoding="utf-8")

    return 0


if __name__ == "__main__":
    sys.exit(main(sys.argv))


class XMLTestCase(unittest.TestCase):
    def assertElementsEqual(
        self,
        got: ET.Element,
        expected: ET.Element,
        msg: typing.Optional[str] = None,
    ) -> None:
        ET.indent(got)
        ET.indent(expected)
        self.assertEqual(
            ET.tostring(got, "unicode"),
            ET.tostring(expected, "unicode"),
            msg,
        )

    def setUp(self) -> None:
        self.addTypeEqualityFunc(ET.Element, self.assertElementsEqual)


class TestCompileReferenceRe(unittest.TestCase):
    def setUp(self) -> None:
        self.compiler = PatternCompiler(["Matthew"])

    def test_basic_operation(self) -> None:
        regex = compile_reference_re(
            self.compiler, ["BOOK CHAPTER:VERSE"], [], []
        )

        matches = list(
            regex.finditer("as seen in Matthew 22:37 of the New Testament")
        )
        self.assertEqual(len(matches), 1)
        self.assertEqual(matches[0].group(0), "Matthew 22:37")
        self.assertEqual(matches[0].group("AbsolutePatterns_0_BOOK"), "Matthew")
        self.assertEqual(matches[0].group("AbsolutePatterns_0_CHAPTER"), "22")
        self.assertEqual(matches[0].group("AbsolutePatterns_0_VERSE"), "37")

    def test_reject_absolute_pattern_without_book(self) -> None:
        with self.assertRaises(MissingToken) as cm:
            compile_reference_re(self.compiler, ["CHAPTER:VERSE"], [], [])
        self.assertEqual(cm.exception.required_token, BOOK)
        self.assertEqual(cm.exception.requiring_token, None)
        self.assertEqual(cm.exception.pattern, "CHAPTER:VERSE")

    def test_reject_absolute_pattern_with_verse_without_chapter(self) -> None:
        with self.assertRaises(MissingToken) as cm:
            compile_reference_re(self.compiler, ["BOOK VERSE"], [], [])
        self.assertEqual(cm.exception.required_token, CHAPTER)
        self.assertEqual(cm.exception.requiring_token, None)
        self.assertEqual(cm.exception.pattern, "BOOK VERSE")

        with self.assertRaises(MissingToken) as cm:
            compile_reference_re(
                self.compiler,
                ["BOOK FROM_VERSE-TO_VERSE"],
                [],
                [],
            )
        self.assertEqual(cm.exception.required_token, CHAPTER)
        self.assertEqual(cm.exception.requiring_token, None)
        self.assertEqual(cm.exception.pattern, "BOOK FROM_VERSE-TO_VERSE")

    def test_reject_local_relative_pattern_with_book(self) -> None:
        with self.assertRaises(UnwantedToken) as cm:
            compile_reference_re(self.compiler, [], ["BOOK CHAPTER:VERSE"], [])
        self.assertEqual(cm.exception.token, BOOK)
        self.assertEqual(cm.exception.pattern, "BOOK CHAPTER:VERSE")

    def test_reject_reference_relative_pattern_with_book(self) -> None:
        with self.assertRaises(UnwantedToken) as cm:
            compile_reference_re(self.compiler, [], [], ["BOOK CHAPTER:VERSE"])
        self.assertEqual(cm.exception.token, BOOK)
        self.assertEqual(cm.exception.pattern, "BOOK CHAPTER:VERSE")

    def test_match_verse_ranges(self) -> None:
        regex = compile_reference_re(
            self.compiler,
            ["BOOK CHAPTER:FROM_VERSE-TO_VERSE", "BOOK CHAPTER:VERSE"],
            [],
            [],
        )

        matches = list(
            regex.finditer("as seen in Matthew 22:37-40 of the New Testament")
        )
        self.assertEqual(len(matches), 1)
        self.assertEqual(matches[0].group(0), "Matthew 22:37-40")
        self.assertEqual(matches[0].group("AbsolutePatterns_0_BOOK"), "Matthew")
        self.assertEqual(matches[0].group("AbsolutePatterns_0_CHAPTER"), "22")
        self.assertEqual(
            matches[0].group("AbsolutePatterns_0_FROM_VERSE"), "37"
        )
        self.assertEqual(matches[0].group("AbsolutePatterns_0_TO_VERSE"), "40")

    def test_reject_reused_tokens(self) -> None:
        with self.assertRaises(ReusedToken) as cm:
            compile_reference_re(
                self.compiler, ["BOOK FROM_CHAPTER-FROM_CHAPTER"], [], []
            )
        self.assertEqual(cm.exception.token, FROM_CHAPTER)
        self.assertEqual(cm.exception.pattern, "BOOK FROM_CHAPTER-FROM_CHAPTER")

    def test_reject_incompatible_chapter_tokens(self) -> None:
        with self.assertRaises(IncompatibleTokens) as cm:
            compile_reference_re(
                self.compiler, ["BOOK FROM_CHAPTER-CHAPTER"], [], []
            )
        self.assertEqual(cm.exception.token1, CHAPTER)
        self.assertEqual(cm.exception.token2, FROM_CHAPTER)
        self.assertEqual(cm.exception.pattern, "BOOK FROM_CHAPTER-CHAPTER")

        with self.assertRaises(IncompatibleTokens) as cm:
            compile_reference_re(
                self.compiler, ["BOOK CHAPTER-TO_CHAPTER"], [], []
            )
        self.assertEqual(cm.exception.token1, CHAPTER)
        self.assertEqual(cm.exception.token2, TO_CHAPTER)
        self.assertEqual(cm.exception.pattern, "BOOK CHAPTER-TO_CHAPTER")

    def test_reject_incompatible_verse_tokens(self) -> None:
        with self.assertRaises(IncompatibleTokens) as cm:
            compile_reference_re(
                self.compiler, ["BOOK CHAPTER:FROM_VERSE-VERSE"], [], []
            )
        self.assertEqual(cm.exception.token1, FROM_VERSE)
        self.assertEqual(cm.exception.token2, VERSE)
        self.assertEqual(cm.exception.pattern, "BOOK CHAPTER:FROM_VERSE-VERSE")

        with self.assertRaises(IncompatibleTokens) as cm:
            compile_reference_re(
                self.compiler, ["BOOK CHAPTER:VERSE-TO_VERSE"], [], []
            )
        self.assertEqual(cm.exception.token1, TO_VERSE)
        self.assertEqual(cm.exception.token2, VERSE)
        self.assertEqual(cm.exception.pattern, "BOOK CHAPTER:VERSE-TO_VERSE")

    def test_reject_verse_in_chapter_range(self) -> None:
        with self.assertRaises(IncompatibleTokens) as cm:
            compile_reference_re(
                self.compiler, ["BOOK FROM_CHAPTER-TO_CHAPTER:VERSE"], [], []
            )
        self.assertIn(cm.exception.token1, [FROM_CHAPTER, TO_CHAPTER])
        self.assertEqual(cm.exception.token2, VERSE)
        self.assertEqual(
            cm.exception.pattern, "BOOK FROM_CHAPTER-TO_CHAPTER:VERSE"
        )

    def test_reject_half_chapter_range_to(self) -> None:
        with self.assertRaises(MissingToken) as cm:
            compile_reference_re(self.compiler, ["BOOK FROM_CHAPTER"], [], [])
        self.assertEqual(cm.exception.requiring_token, FROM_CHAPTER)
        self.assertEqual(cm.exception.required_token, TO_CHAPTER)

        with self.assertRaises(MissingToken) as cm:
            compile_reference_re(self.compiler, ["BOOK TO_CHAPTER"], [], [])
        self.assertEqual(cm.exception.requiring_token, TO_CHAPTER)
        self.assertEqual(cm.exception.required_token, FROM_CHAPTER)

    def test_reject_half_verse_range_to(self) -> None:
        with self.assertRaises(MissingToken) as cm:
            compile_reference_re(
                self.compiler, ["BOOK CHAPTER:FROM_VERSE"], [], []
            )
        self.assertEqual(cm.exception.requiring_token, FROM_VERSE)
        self.assertEqual(cm.exception.required_token, TO_VERSE)

        with self.assertRaises(MissingToken) as cm:
            compile_reference_re(
                self.compiler, ["BOOK CHAPTER:TO_VERSE"], [], []
            )
        self.assertEqual(cm.exception.requiring_token, TO_VERSE)
        self.assertEqual(cm.exception.required_token, FROM_VERSE)

    def test_no_reference_patterns_means_no_matches(self) -> None:
        regex = compile_reference_re(self.compiler, [], [], [])

        self.assertIsNone(regex.search("hello world"))

    def test_no_ref_chain_patterns_means_no_matches(self) -> None:
        links_re, items_re = compile_reference_chain_re(self.compiler, [], [])

        self.assertIsNone(links_re.search("hello world"))
        self.assertIsNone(items_re.search("hello world"))


class TestOsisId(unittest.TestCase):
    def test_basic_operation(self) -> None:
        actual = OsisId.from_raw("Gen.1.1")
        expected = OsisId("Gen", 1, 1)
        self.assertEqual(actual, expected)
        self.assertEqual(str(actual), "Gen.1.1")

    def test_chapter_id(self) -> None:
        actual = OsisId.from_raw("Gen.1")
        expected = OsisId("Gen", 1, None)
        self.assertEqual(actual, expected)
        self.assertEqual(str(actual), "Gen.1")

    def test_book_id(self) -> None:
        actual = OsisId.from_raw("Gen")
        expected = OsisId("Gen", None, None)
        self.assertEqual(actual, expected)
        self.assertEqual(str(actual), "Gen")

    def test_reject_too_many_parts(self) -> None:
        with self.assertRaises(ValueError) as cm:
            OsisId.from_raw("Gen.1.1.1")

        self.assertEqual(
            str(cm.exception), "osisID must be book.chapter.verse: 'Gen.1.1.1'"
        )

    def test_reject_unknown_book(self) -> None:
        with self.assertRaises(ValueError) as cm:
            OsisId.from_raw("UnknownBook.1.1")

        self.assertEqual(
            str(cm.exception),
            "Unrecognised OSIS book ID 'UnknownBook': 'UnknownBook.1.1'",
        )

    def test_reject_non_numeric_chapter(self) -> None:
        with self.assertRaises(ValueError) as cm:
            OsisId.from_raw("Gen.Blorp.1")

        self.assertEqual(
            str(cm.exception),
            "invalid literal for int() with base 10: 'Blorp'",
        )

    def test_reject_zero_chapter(self) -> None:
        with self.assertRaises(ValueError) as cm:
            OsisId.from_raw("Gen.0.1")

        self.assertEqual(
            str(cm.exception),
            "Chapter must be natural number, not 0 in osisID 'Gen.0.1'",
        )

    def test_reject_negative_chapter(self) -> None:
        with self.assertRaises(ValueError) as cm:
            OsisId.from_raw("Gen.-1.1")

        self.assertEqual(
            str(cm.exception),
            "Chapter must be natural number, not -1 in osisID 'Gen.-1.1'",
        )

    def test_reject_non_numeric_verse(self) -> None:
        with self.assertRaises(ValueError) as cm:
            OsisId.from_raw("Gen.1.Blorp")

        self.assertEqual(
            str(cm.exception),
            "invalid literal for int() with base 10: 'Blorp'",
        )

    def test_reject_zero_verse(self) -> None:
        with self.assertRaises(ValueError) as cm:
            OsisId.from_raw("Gen.1.0")

        self.assertEqual(
            str(cm.exception),
            "Verse must be natural number, not 0 in osisID 'Gen.1.0'",
        )

    def test_reject_negative_verse(self) -> None:
        with self.assertRaises(ValueError) as cm:
            OsisId.from_raw("Gen.1.-1")

        self.assertEqual(
            str(cm.exception),
            "Verse must be natural number, not -1 in osisID 'Gen.1.-1'",
        )


class TestReferenceMatchToOsisId(unittest.TestCase):
    def setUp(self) -> None:
        self.book_name_to_osis = {"Matthew": "Matt", "Jasher": None}
        self.compiler = PatternCompiler(self.book_name_to_osis.keys())
        self.reference_re = compile_reference_re(
            self.compiler,
            [
                "BOOK FROM_CHAPTER:FROM_VERSE-TO_CHAPTER:TO_VERSE",
                "BOOK CHAPTER:FROM_VERSE-TO_VERSE",
                "BOOK CHAPTER:VERSE",
                "BOOK FROM_CHAPTER-TO_CHAPTER",
                "BOOK CHAPTER",
                "BOOK",
            ],
            [
                "FROM_CHAPTER:FROM_VERSE-TO_CHAPTER:TO_VERSE",
                "CHAPTER:FROM_VERSE-TO_VERSE",
                "CHAPTER:VERSE",
                "chapters FROM_CHAPTER-TO_CHAPTER",
                "chapter CHAPTER",
                "verses FROM_VERSE-TO_VERSE",
                "verse VERSE",
            ],
            ["Idem CHAPTER:VERSE", ", NUMBER", ", FROM_NUMBER-TO_NUMBER"],
        )
        self.context = ReferenceContext(
            OsisId("Gen", 1, 1), OsisId("Exod", 2, 2)
        )

    def test_absolute_verse_reference(self) -> None:
        ref_match = self.reference_re.fullmatch("Matthew 22:37")
        assert ref_match is not None

        self.assertEqual(
            reference_match_to_osis_ref(
                self.book_name_to_osis,
                self.context,
                ref_match,
            ),
            "Matt.22.37",
        )

    def test_absolute_verse_range_reference(self) -> None:
        ref_match = self.reference_re.fullmatch("Matthew 22:37-40")
        assert ref_match is not None

        self.assertEqual(
            reference_match_to_osis_ref(
                self.book_name_to_osis,
                self.context,
                ref_match,
            ),
            "Matt.22.37-Matt.22.40",
        )

    def test_invalid_absolute_verse_range_reference(self) -> None:
        ref_match = self.reference_re.fullmatch("Matthew 22:40-37")
        assert ref_match is not None

        with self.assertLogs(level="WARNING") as cm:
            self.assertEqual(
                reference_match_to_osis_ref(
                    self.book_name_to_osis,
                    self.context,
                    ref_match,
                ),
                None,
            )

        self.assertEqual(
            cm.output,
            [
                "WARNING:root:In Gen.1.1, reference 'Matthew 22:40-37' "
                "has verse numbers in the wrong order"
            ],
        )

    def test_absolute_chapter_and_verse_range_reference(self) -> None:
        ref_match = self.reference_re.fullmatch("Matthew 22:37-23:1")
        assert ref_match is not None

        self.assertEqual(
            reference_match_to_osis_ref(
                self.book_name_to_osis,
                self.context,
                ref_match,
            ),
            "Matt.22.37-Matt.23.1",
        )

    def test_absolute_chapter_and_invalid_verse_range_reference(self) -> None:
        ref_match = self.reference_re.fullmatch("Matthew 22:40-22:37")
        assert ref_match is not None

        with self.assertLogs(level="WARNING") as cm:
            self.assertEqual(
                reference_match_to_osis_ref(
                    self.book_name_to_osis,
                    self.context,
                    ref_match,
                ),
                None,
            )

        self.assertEqual(
            cm.output,
            [
                "WARNING:root:In Gen.1.1, reference 'Matthew 22:40-22:37' "
                "has verse numbers in the wrong order",
            ],
        )

    def test_invalid_absolute_chapter_and_verse_range_reference(self) -> None:
        ref_match = self.reference_re.fullmatch("Matthew 23:40-22:37")
        assert ref_match is not None

        with self.assertLogs(level="WARNING") as cm:
            self.assertEqual(
                reference_match_to_osis_ref(
                    self.book_name_to_osis,
                    self.context,
                    ref_match,
                ),
                None,
            )

        self.assertEqual(
            cm.output,
            [
                "WARNING:root:In Gen.1.1, reference 'Matthew 23:40-22:37' "
                "has chapter numbers in the wrong order",
            ],
        )

    def test_absolute_chapter_range_reference(self) -> None:
        ref_match = self.reference_re.fullmatch("Matthew 22-23")
        assert ref_match is not None

        self.assertEqual(
            reference_match_to_osis_ref(
                self.book_name_to_osis,
                self.context,
                ref_match,
            ),
            "Matt.22-Matt.23",
        )

    def test_reversed_absolute_chapter_range_reference(self) -> None:
        ref_match = self.reference_re.fullmatch("Matthew 23-22")
        assert ref_match is not None

        with self.assertLogs(level="WARNING") as cm:
            self.assertEqual(
                reference_match_to_osis_ref(
                    self.book_name_to_osis,
                    self.context,
                    ref_match,
                ),
                None,
            )

        self.assertEqual(
            cm.output,
            [
                "WARNING:root:In Gen.1.1, reference 'Matthew 23-22' "
                "has chapter numbers in the wrong order",
            ],
        )

    def test_equal_absolute_chapter_range_reference(self) -> None:
        ref_match = self.reference_re.fullmatch("Matthew 22-22")
        assert ref_match is not None

        with self.assertLogs(level="WARNING") as cm:
            self.assertEqual(
                reference_match_to_osis_ref(
                    self.book_name_to_osis,
                    self.context,
                    ref_match,
                ),
                None,
            )

        self.assertEqual(
            cm.output,
            [
                "WARNING:root:In Gen.1.1, reference 'Matthew 22-22' "
                "has identical chapter numbers",
            ],
        )

    def test_reversed_absolute_verse_range_reference(self) -> None:
        ref_match = self.reference_re.fullmatch("Matthew 22:40-37")
        assert ref_match is not None

        with self.assertLogs(level="WARNING") as cm:
            self.assertEqual(
                reference_match_to_osis_ref(
                    self.book_name_to_osis,
                    self.context,
                    ref_match,
                ),
                None,
            )

        self.assertEqual(
            cm.output,
            [
                "WARNING:root:In Gen.1.1, reference 'Matthew 22:40-37' "
                "has verse numbers in the wrong order",
            ],
        )

    def test_equal_absolute_verse_range_reference(self) -> None:
        ref_match = self.reference_re.fullmatch("Matthew 22:37-37")
        assert ref_match is not None

        with self.assertLogs(level="WARNING") as cm:
            self.assertEqual(
                reference_match_to_osis_ref(
                    self.book_name_to_osis,
                    self.context,
                    ref_match,
                ),
                None,
            )

        self.assertEqual(
            cm.output,
            [
                "WARNING:root:In Gen.1.1, reference 'Matthew 22:37-37' "
                "has identical verse numbers",
            ],
        )

    def test_absolute_book_reference(self) -> None:
        ref_match = self.reference_re.fullmatch("Matthew")
        assert ref_match is not None

        self.assertEqual(
            reference_match_to_osis_ref(
                self.book_name_to_osis,
                self.context,
                ref_match,
            ),
            "Matt",
        )

    def test_book_relative_verse_reference(self) -> None:
        ref_match = self.reference_re.fullmatch("22:37")
        assert ref_match is not None

        self.assertEqual(
            reference_match_to_osis_ref(
                self.book_name_to_osis,
                self.context,
                ref_match,
            ),
            "Gen.22.37",
        )

    def test_book_relative_verse_range_reference(self) -> None:
        ref_match = self.reference_re.fullmatch("22:37-40")
        assert ref_match is not None

        self.assertEqual(
            reference_match_to_osis_ref(
                self.book_name_to_osis,
                self.context,
                ref_match,
            ),
            "Gen.22.37-Gen.22.40",
        )

    def test_book_relative_chapter_and_verse_range_reference(self) -> None:
        ref_match = self.reference_re.fullmatch("22:37-23:13")
        assert ref_match is not None

        self.assertEqual(
            reference_match_to_osis_ref(
                self.book_name_to_osis,
                self.context,
                ref_match,
            ),
            "Gen.22.37-Gen.23.13",
        )

    def test_book_relative_chapter_reference(self) -> None:
        ref_match = self.reference_re.fullmatch("chapter 22")
        assert ref_match is not None

        self.assertEqual(
            reference_match_to_osis_ref(
                self.book_name_to_osis,
                self.context,
                ref_match,
            ),
            "Gen.22",
        )

    def test_book_relative_chapter_range_reference(self) -> None:
        ref_match = self.reference_re.fullmatch("chapters 22-23")
        assert ref_match is not None

        self.assertEqual(
            reference_match_to_osis_ref(
                self.book_name_to_osis,
                self.context,
                ref_match,
            ),
            "Gen.22-Gen.23",
        )

    def test_chapter_relative_verse_reference(self) -> None:
        ref_match = self.reference_re.fullmatch("verse 37")
        assert ref_match is not None

        self.assertEqual(
            reference_match_to_osis_ref(
                self.book_name_to_osis,
                self.context,
                ref_match,
            ),
            "Gen.1.37",
        )

    def test_chapter_relative_verse_range_reference(self) -> None:
        ref_match = self.reference_re.fullmatch("verses 37-40")
        assert ref_match is not None

        self.assertEqual(
            reference_match_to_osis_ref(
                self.book_name_to_osis,
                self.context,
                ref_match,
            ),
            "Gen.1.37-Gen.1.40",
        )

    def test_ignore_absolute_reference_to_ignored_book(self) -> None:
        ref_match = self.reference_re.fullmatch("Jasher")
        assert ref_match is not None

        self.assertEqual(
            reference_match_to_osis_ref(
                self.book_name_to_osis,
                self.context,
                ref_match,
            ),
            None,
        )

    def test_ignore_absolute_reference_to_chapter_in_ignored_book(self) -> None:
        ref_match = self.reference_re.fullmatch("Jasher 12")
        assert ref_match is not None

        self.assertEqual(
            reference_match_to_osis_ref(
                self.book_name_to_osis,
                self.context,
                ref_match,
            ),
            None,
        )

    def test_ignore_absolute_reference_to_chapters_in_ignored_book(
        self,
    ) -> None:
        ref_match = self.reference_re.fullmatch("Jasher 12-34")
        assert ref_match is not None

        self.assertEqual(
            reference_match_to_osis_ref(
                self.book_name_to_osis,
                self.context,
                ref_match,
            ),
            None,
        )

    def test_ignore_absolute_reference_to_verse_in_ignored_book(self) -> None:
        ref_match = self.reference_re.fullmatch("Jasher 12:34")
        assert ref_match is not None

        self.assertEqual(
            reference_match_to_osis_ref(
                self.book_name_to_osis,
                self.context,
                ref_match,
            ),
            None,
        )

    def test_interpret_reference_relative_reference(self) -> None:
        ref_match = self.reference_re.fullmatch("Idem 3:4")
        assert ref_match is not None

        self.assertEqual(
            reference_match_to_osis_ref(
                self.book_name_to_osis,
                self.context,
                ref_match,
            ),
            "Exod.3.4",
        )

    def test_ignore_reference_relative_reference_without_previous(self) -> None:
        # Make a context with no last_reference.
        context = ReferenceContext(OsisId("Gen", 1, 1))

        ref_match = self.reference_re.fullmatch("Idem 3:4")
        assert ref_match is not None

        with self.assertLogs(level="WARNING") as cm:
            self.assertIsNone(
                reference_match_to_osis_ref(
                    self.book_name_to_osis, context, ref_match
                )
            )

        self.assertEqual(
            cm.output,
            [
                "WARNING:root:In Gen.1.1, "
                "reference-relative reference 'Idem 3:4' "
                "occurs before first reference?"
            ],
        )

    def test_interpret_number_token_as_verse(self) -> None:
        ref_match = self.reference_re.fullmatch(", 3")
        assert ref_match is not None

        self.assertEqual(
            reference_match_to_osis_ref(
                self.book_name_to_osis,
                self.context,
                ref_match,
            ),
            "Exod.2.3",
        )

    def test_interpret_number_token_as_chapter(self) -> None:
        # Make a context whose last reference is a chapter.
        context = ReferenceContext(OsisId("Gen", 1, 1), OsisId("Exod", 2, None))

        ref_match = self.reference_re.fullmatch(", 3")
        assert ref_match is not None

        self.assertEqual(
            reference_match_to_osis_ref(
                self.book_name_to_osis,
                context,
                ref_match,
            ),
            "Exod.3",
        )


class TestAddReferencesToText(XMLTestCase):
    def setUp(self) -> None:
        super().setUp()
        self.context = ReferenceContext()
        self.book_name_to_osis = {
            "Matthew": "Matt",
            "Deuteronomy": "Deut",
            "1 Corinthians": "1Cor",
        }
        self.compiler = PatternCompiler(self.book_name_to_osis.keys())
        self.maxDiff = None

    def test_basic_operation(self) -> None:
        reference_re = compile_reference_re(
            self.compiler,
            ["BOOK CHAPTER:FROM_VERSE-TO_VERSE"],
            [],
            [],
        )
        links_re, items_re = compile_reference_chain_re(self.compiler, [], [])

        actual = add_references_to_text(
            "In Matthew 22:37-40, Jesus summarises the Hebrew Scriptures.",
            reference_re,
            links_re,
            items_re,
            self.book_name_to_osis,
            self.context,
        )
        expected = ET.XML(
            f"<wrapper xmlns='{OSIS_NS}'>"
            "In "
            "<reference type='source' osisRef='Matt.22.37-Matt.22.40'>"
            "Matthew 22:37-40"
            "</reference>"
            ", Jesus summarises the Hebrew Scriptures."
            "</wrapper>"
        )
        self.assertEqual(actual, expected)

    def test_no_references_found(self) -> None:
        reference_re = compile_reference_re(
            self.compiler, ["BOOK CHAPTER:VERSE"], [], []
        )
        links_re, items_re = compile_reference_chain_re(self.compiler, [], [])

        actual = add_references_to_text(
            "LXX, Vulgate, and Syriac; Hebrew “Very well!” or “Therefore:”",
            reference_re,
            links_re,
            items_re,
            self.book_name_to_osis,
            self.context,
        )
        expected = ET.XML(
            f"<wrapper xmlns='{OSIS_NS}'>"
            "LXX, Vulgate, and Syriac; Hebrew “Very well!” or “Therefore:”"
            "</wrapper>"
        )
        self.assertEqual(actual, expected)

    def test_add_chained_references(self) -> None:
        reference_re = compile_reference_re(
            self.compiler, ["BOOK CHAPTER:VERSE"], [], []
        )
        links_re, items_re = compile_reference_chain_re(
            self.compiler, [", and", ","], ["CHAPTER:VERSE"]
        )

        actual = add_references_to_text(
            "Literally Expel the evil from among you; "
            "Deuteronomy 13:5, 17:7, 19:19, 21:21, 22:21, 22:24, and 24:7",
            reference_re,
            links_re,
            items_re,
            self.book_name_to_osis,
            ReferenceContext(OsisId("1Cor", 5, 13)),
        )
        expected = ET.XML(
            f"<wrapper xmlns='{OSIS_NS}'>"
            "Literally Expel the evil from among you; "
            "<reference type='source' osisRef='Deut.13.5'>Deuteronomy 13:5</reference>, "
            "<reference type='source' osisRef='Deut.17.7'>17:7</reference>, "
            "<reference type='source' osisRef='Deut.19.19'>19:19</reference>, "
            "<reference type='source' osisRef='Deut.21.21'>21:21</reference>, "
            "<reference type='source' osisRef='Deut.22.21'>22:21</reference>, "
            "<reference type='source' osisRef='Deut.22.24'>22:24</reference>, "
            "and <reference type='source' osisRef='Deut.24.7'>24:7</reference>"
            "</wrapper>"
        )
        self.assertEqual(actual, expected)

    def test_ignore_links_at_the_start_of_the_text(self) -> None:
        reference_re = compile_reference_re(
            self.compiler, ["BOOK CHAPTER:VERSE"], [], []
        )
        links_re, items_re = compile_reference_chain_re(
            self.compiler, [",", "and"], ["NUMBER"]
        )

        actual = add_references_to_text(
            "and 300 concubines",
            reference_re,
            links_re,
            items_re,
            self.book_name_to_osis,
            ReferenceContext(OsisId("1Kgs", 11, 3), OsisId("Gen", 1, 1)),
        )
        expected = ET.XML(
            f"<wrapper xmlns='{OSIS_NS}'>and 300 concubines</wrapper>"
        )
        self.assertEqual(actual, expected)

    def test_chained_references_do_not_interfere_with_regular_refs(
        self,
    ) -> None:
        reference_re = compile_reference_re(
            self.compiler, ["BOOK CHAPTER:VERSE"], [], []
        )
        links_re, items_re = compile_reference_chain_re(
            self.compiler, [","], ["CHAPTER"]
        )

        # If the reference-chain detection is implemented badly,
        # the ", 1" will be mis-detected as a reference to Matthew 1,
        # and the "1 Corinthians" reference may be missed entirely.

        actual = add_references_to_text(
            "See Matthew 22:37, 1 Corinthians 5:13",
            reference_re,
            links_re,
            items_re,
            self.book_name_to_osis,
            self.context,
        )
        expected = ET.XML(
            f"<wrapper xmlns='{OSIS_NS}'>"
            "See "
            "<reference type='source' osisRef='Matt.22.37'>"
            "Matthew 22:37"
            "</reference>, "
            "<reference type='source' osisRef='1Cor.5.13'>"
            "1 Corinthians 5:13"
            "</reference>"
            "</wrapper>"
        )
        self.assertEqual(actual, expected)


class TestAppendTextToElement(XMLTestCase):
    def test_append_text_to_empty_element(self) -> None:
        actual = ET.Element("div")
        append_text_to_element(actual, "hello")

        expected = ET.XML("<div>hello</div>")

        self.assertEqual(actual, expected)

    def test_append_none_to_empty_element(self) -> None:
        actual = ET.Element("div")
        append_text_to_element(actual, None)

        expected = ET.XML("<div/>")

        self.assertEqual(actual, expected)

    def test_append_text_to_wrapper_text(self) -> None:
        actual = ET.XML("<div>hello </div>")
        append_text_to_element(actual, "world")

        expected = ET.XML("<div>hello world</div>")

        self.assertEqual(actual, expected)

    def test_append_none_to_wrapper_text(self) -> None:
        actual = ET.XML("<div>hello</div>")
        append_text_to_element(actual, None)

        expected = ET.XML("<div>hello</div>")

        self.assertEqual(actual, expected)

    def test_append_text_to_tailless_child(self) -> None:
        actual = ET.XML("<div>hello <i>there</i></div>")
        append_text_to_element(actual, " world")

        expected = ET.XML("<div>hello <i>there</i> world</div>")

        self.assertEqual(actual, expected)

    def test_append_none_to_tailless_child(self) -> None:
        actual = ET.XML("<div>hello <i>there</i></div>")
        append_text_to_element(actual, None)

        expected = ET.XML("<div>hello <i>there</i></div>")

        self.assertEqual(actual, expected)

    def test_append_text_to_child_tail(self) -> None:
        actual = ET.XML("<div>hello <i>there</i> </div>")
        append_text_to_element(actual, "world")

        expected = ET.XML("<div>hello <i>there</i> world</div>")

        self.assertEqual(actual, expected)

    def test_append_none_to_child_tail(self) -> None:
        actual = ET.XML("<div>hello <i>there</i> </div>")
        append_text_to_element(actual, None)

        expected = ET.XML("<div>hello <i>there</i> </div>")

        self.assertEqual(actual, expected)


class TestAddReferences(XMLTestCase):
    def setUp(self) -> None:
        super().setUp()
        self.book_name_to_osis = {"Matthew": "Matt"}
        self.compiler = PatternCompiler(self.book_name_to_osis.keys())

    def test_parse_absolute_reference_in_text(self) -> None:
        reference_re = compile_reference_re(
            self.compiler, ["BOOK CHAPTER:VERSE"], [], []
        )
        links_re, items_re = compile_reference_chain_re(self.compiler, [], [])
        actual = ET.XML(
            """
              <p xmlns="http://www.bibletechnologies.net/2003/OSIS/namespace">
                  In
                  Matthew 22:37,
                  Jesus summarises the Hebrew Scriptures.
              </p>
            """
        )

        add_references(
            actual, reference_re, links_re, items_re, self.book_name_to_osis
        )

        expected = ET.XML(
            """
              <p xmlns="http://www.bibletechnologies.net/2003/OSIS/namespace">
                  In
                  <reference type="source" osisRef="Matt.22.37">Matthew 22:37</reference>,
                  Jesus summarises the Hebrew Scriptures.
              </p>
            """
        )

        self.assertEqual(actual, expected)

    def test_parse_absolute_reference_in_tail(self) -> None:
        reference_re = compile_reference_re(
            self.compiler, ["BOOK CHAPTER:VERSE"], [], []
        )
        links_re, items_re = compile_reference_chain_re(self.compiler, [], [])
        actual = ET.XML(
            """
              <p xmlns="http://www.bibletechnologies.net/2003/OSIS/namespace">
                  <note placement="foot">A footnote</note>
                  In Matthew 22:37,
                  Jesus summarises the Hebrew Scriptures.
              </p>
            """
        )

        add_references(
            actual, reference_re, links_re, items_re, self.book_name_to_osis
        )

        expected = ET.XML(
            """
              <p xmlns="http://www.bibletechnologies.net/2003/OSIS/namespace">
                  <note placement="foot">A footnote</note>
                  In <reference type="source" osisRef="Matt.22.37">Matthew 22:37</reference>,
                  Jesus summarises the Hebrew Scriptures.
              </p>
            """
        )

        self.assertEqual(actual, expected)

    def test_passthrough_existing_references(self) -> None:
        reference_re = compile_reference_re(
            self.compiler, ["BOOK CHAPTER:VERSE"], [], []
        )
        links_re, items_re = compile_reference_chain_re(self.compiler, [], [])
        actual = ET.XML(
            """
              <p xmlns="http://www.bibletechnologies.net/2003/OSIS/namespace">
                  In the Bible,
                  <reference type="source" osisRef="Matt.22.37-Matt.22.40">
                  Jesus summarises the Hebrew Scriptures
                  </reference>.
              </p>
            """
        )

        add_references(
            actual, reference_re, links_re, items_re, self.book_name_to_osis
        )

        expected = ET.XML(
            """
              <p xmlns="http://www.bibletechnologies.net/2003/OSIS/namespace">
                  In the Bible,
                  <reference type="source" osisRef="Matt.22.37-Matt.22.40">
                  Jesus summarises the Hebrew Scriptures
                  </reference>.
              </p>
            """
        )

        self.assertEqual(actual, expected)

    def test_fix_absolute_references_without_targets(self) -> None:
        reference_re = compile_reference_re(
            self.compiler, ["BOOK CHAPTER:VERSE"], [], []
        )
        links_re, items_re = compile_reference_chain_re(self.compiler, [], [])
        actual = ET.XML(
            """
              <p xmlns="http://www.bibletechnologies.net/2003/OSIS/namespace">
                  In
                  <reference type="source">Matthew 22:37</reference>,
                  Jesus summarises the Hebrew Scriptures.
              </p>
            """
        )

        add_references(
            actual, reference_re, links_re, items_re, self.book_name_to_osis
        )

        expected = ET.XML(
            """
              <p xmlns="http://www.bibletechnologies.net/2003/OSIS/namespace">
                  In
                  <reference type="source" osisRef="Matt.22.37">Matthew 22:37</reference>,
                  Jesus summarises the Hebrew Scriptures.
              </p>
            """
        )

        self.assertEqual(actual, expected)

    def test_drop_broken_references_without_targets(self) -> None:
        reference_re = compile_reference_re(self.compiler, [], [], [])
        links_re, items_re = compile_reference_chain_re(self.compiler, [], [])
        actual = ET.XML(
            """
              <p xmlns="http://www.bibletechnologies.net/2003/OSIS/namespace">
                  <reference type="source">Hello</reference> world
              </p>
            """
        )

        add_references(
            actual, reference_re, links_re, items_re, self.book_name_to_osis
        )

        expected = ET.XML(
            """
              <p xmlns="http://www.bibletechnologies.net/2003/OSIS/namespace">
                  Hello world
              </p>
            """
        )

        self.assertEqual(actual, expected)

    def test_parse_reference_in_tail_of_dropped_reference(self) -> None:
        reference_re = compile_reference_re(
            self.compiler, ["BOOK CHAPTER:VERSE"], [], []
        )
        links_re, items_re = compile_reference_chain_re(self.compiler, [], [])
        actual = ET.XML(
            """
              <p xmlns="http://www.bibletechnologies.net/2003/OSIS/namespace">
                  <reference type="source">In</reference>
                  Matthew 22:37,
                  Jesus summarises the Hebrew Scriptures.
              </p>
            """
        )

        add_references(
            actual, reference_re, links_re, items_re, self.book_name_to_osis
        )

        expected = ET.XML(
            """
              <p xmlns="http://www.bibletechnologies.net/2003/OSIS/namespace">
                  In
                  <reference type="source" osisRef="Matt.22.37">Matthew 22:37</reference>,
                  Jesus summarises the Hebrew Scriptures.
              </p>
            """
        )

        self.assertEqual(actual, expected)

    def test_add_location_relative_references(self) -> None:
        reference_re = compile_reference_re(
            self.compiler, [], ["verse VERSE"], []
        )
        links_re, items_re = compile_reference_chain_re(self.compiler, [], [])
        actual = ET.XML(
            """
              <p xmlns="http://www.bibletechnologies.net/2003/OSIS/namespace"
                  osisID="Matt.22.37">
                  This passage continues to
                  verse 40.
              </p>
            """
        )

        add_references(
            actual, reference_re, links_re, items_re, self.book_name_to_osis
        )

        expected = ET.XML(
            """
              <p xmlns="http://www.bibletechnologies.net/2003/OSIS/namespace"
                  osisID="Matt.22.37">
                  This passage continues to
                  <reference type="source" osisRef="Matt.22.40">verse 40</reference>.
              </p>
            """
        )

        self.assertEqual(actual, expected)

    def test_add_reference_relative_references(self) -> None:
        reference_re = compile_reference_re(
            self.compiler, ["BOOK CHAPTER:VERSE"], [], ["verse VERSE"]
        )
        links_re, items_re = compile_reference_chain_re(self.compiler, [], [])
        actual = ET.XML(
            """
              <p xmlns="http://www.bibletechnologies.net/2003/OSIS/namespace">
                  This passage continues from
                  Matthew 22:37
                  to
                  verse 40.
              </p>
            """
        )

        add_references(
            actual, reference_re, links_re, items_re, self.book_name_to_osis
        )

        expected = ET.XML(
            """
              <p xmlns="http://www.bibletechnologies.net/2003/OSIS/namespace">
                  This passage continues from
                  <reference type="source" osisRef="Matt.22.37">Matthew 22:37</reference>
                  to
                  <reference type="source" osisRef="Matt.22.40">verse 40</reference>.
              </p>
            """
        )

        self.assertEqual(actual, expected)

    def test_reject_location_relative_ref_before_first_location(self) -> None:
        reference_re = compile_reference_re(
            self.compiler, [], ["verse VERSE"], []
        )
        links_re, items_re = compile_reference_chain_re(self.compiler, [], [])
        actual = ET.XML(
            """
              <p xmlns="http://www.bibletechnologies.net/2003/OSIS/namespace">
                  This passage continues to
                  verse 40.
              </p>
            """
        )

        with self.assertLogs(level="WARNING") as cm:
            add_references(
                actual, reference_re, links_re, items_re, self.book_name_to_osis
            )

        self.assertEqual(
            cm.output,
            [
                "WARNING:root:Got location-relative reference 'verse 40' "
                "before first location?"
            ],
        )

    def test_reject_ref_relative_ref_before_first_ref(self) -> None:
        reference_re = compile_reference_re(
            self.compiler, [], [], ["verse VERSE"]
        )
        links_re, items_re = compile_reference_chain_re(self.compiler, [], [])
        actual = ET.XML(
            """
              <p xmlns="http://www.bibletechnologies.net/2003/OSIS/namespace">
                  This passage continues to
                  verse 40.
              </p>
            """
        )

        with self.assertLogs(level="WARNING") as cm:
            add_references(
                actual, reference_re, links_re, items_re, self.book_name_to_osis
            )

        self.assertEqual(
            cm.output,
            [
                "WARNING:root:In None, reference-relative reference 'verse 40' "
                "occurs before first reference?"
            ],
        )

    def test_handle_multiple_osisIDs(self) -> None:
        reference_re = compile_reference_re(
            self.compiler, [], ["verse VERSE"], []
        )
        links_re, items_re = compile_reference_chain_re(self.compiler, [], [])
        actual = ET.XML(
            """
              <p xmlns="http://www.bibletechnologies.net/2003/OSIS/namespace"
                  osisID="Matt.22.37 Matt.22.38 Matt.22.39 Matt.22.40">
                  From
                  verse 37
                  to
                  verse 40,
                  Jesus talks about the Greatest Commandment.
              </p>
            """
        )

        add_references(
            actual, reference_re, links_re, items_re, self.book_name_to_osis
        )

        expected = ET.XML(
            """
              <p xmlns="http://www.bibletechnologies.net/2003/OSIS/namespace"
                  osisID="Matt.22.37 Matt.22.38 Matt.22.39 Matt.22.40">
                  From
                  <reference type="source" osisRef="Matt.22.37">verse 37</reference>
                  to
                  <reference type="source" osisRef="Matt.22.40">verse 40</reference>,
                  Jesus talks about the Greatest Commandment.
              </p>
            """
        )

        self.assertEqual(actual, expected)
