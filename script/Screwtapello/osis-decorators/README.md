OSIS Decorators
===============

This repository contains various tools for working with [OSIS] documents,
such as Bibles and Bible commentaries.
It's called "OSIS Decorators" because
these tools generally "decorate" the document in some way,
such as adding cross-reference links
or cleaning up unwanted text.
Each tool is in a subdirectory,
along with its documentation,
example configuration,
and any other relevant information.

[OSIS]: https://crosswire.org/osis/

All the tools in this repository require Python 3.6 or higher,
but do not require any other tools or libraries.
