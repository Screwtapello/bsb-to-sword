#!/usr/bin/env python3
import argparse
import logging
import sys
import typing
import unittest
from xml.etree import ElementTree as ET

# The XML namespace of OSIS documents
OSIS_NS = "http://www.bibletechnologies.net/2003/OSIS/namespace"


def append_text_to_element(
    elem: ET.Element, text: typing.Optional[str]
) -> None:
    """
    Appends text after any content in elem.

    This correctly chooses whether to put the new text in
    the elem's .text property, or the .tail of a child element.
    """
    if len(elem) == 0:
        if elem.text is None:
            elem.text = text
        elif text is not None:
            elem.text += text
    else:
        last_child = elem[-1]
        if last_child.tail is None:
            last_child.tail = text
        elif text is not None:
            last_child.tail += text


def remove_footnote_backrefs(elem: ET.Element) -> None:
    """
    Remove "annotateRef" references in annotations.

    In a printed Bible,
    it's helpful to have a reference from a footnote back to the text,
    but in a digital text we have a "back" button
    and circular links just confuse matters.
    """
    reference_tag = str(ET.QName(OSIS_NS, "reference"))

    for each in elem.findall(".//osis:note[osis:reference]", {"osis": OSIS_NS}):
        new_children = ET.Element("new-children")
        new_children.text = each.text

        for child in each:
            if (
                child.tag == reference_tag
                and child.get("type") == "annotateRef"
            ):
                # Skip over this child eleement,
                # but preserve its tail if any.
                append_text_to_element(new_children, child.tail)

            else:
                # It's just an ordinary element, keep it around.
                new_children.append(child)

        each[:] = new_children
        each.text = new_children.text


def main(raw_args: typing.List[str]) -> int:
    parser = argparse.ArgumentParser(
        description="Remove backreferences from footnotes in an OSIS document"
    )
    parser.add_argument(
        "--verbose",
        "-v",
        dest="verbose",
        default=0,
        action="count",
        help="Show more details; can be repeated.",
    )
    parser.add_argument(
        "input",
        metavar="INPUT",
        help="Scan this OSIS XML file for backreferences in footnotes",
        type=argparse.FileType("rb"),
    )
    parser.add_argument(
        "output",
        metavar="OUTPUT",
        help="Write the updated OSIS XML to this file (must not exist yet)",
        type=argparse.FileType("xb"),
    )
    args = parser.parse_args(raw_args[1:])

    logging.basicConfig(
        format="%(levelname)s: %(message)s",
        level=logging.WARNING - (args.verbose * 10),
    )
    logging.captureWarnings(True)

    ET.register_namespace("", OSIS_NS)

    with args.input as input, args.output as output:
        tree = ET.parse(input)
        remove_footnote_backrefs(tree.getroot())
        tree.write(output, encoding="utf-8")

    return 0


if __name__ == "__main__":
    sys.exit(main(sys.argv))


class XMLTestCase(unittest.TestCase):
    def assertElementsEqual(
        self,
        got: ET.Element,
        expected: ET.Element,
        msg: typing.Optional[str] = None,
    ) -> None:
        ET.indent(got)
        ET.indent(expected)
        self.assertEqual(
            ET.tostring(got, "unicode"),
            ET.tostring(expected, "unicode"),
            msg,
        )

    def setUp(self) -> None:
        self.addTypeEqualityFunc(ET.Element, self.assertElementsEqual)


class TestAppendTextToElement(XMLTestCase):
    def test_append_text_to_empty_element(self) -> None:
        actual = ET.Element("div")
        append_text_to_element(actual, "hello")

        expected = ET.XML("<div>hello</div>")

        self.assertEqual(actual, expected)

    def test_append_none_to_empty_element(self) -> None:
        actual = ET.Element("div")
        append_text_to_element(actual, None)

        expected = ET.XML("<div/>")

        self.assertEqual(actual, expected)

    def test_append_text_to_wrapper_text(self) -> None:
        actual = ET.XML("<div>hello </div>")
        append_text_to_element(actual, "world")

        expected = ET.XML("<div>hello world</div>")

        self.assertEqual(actual, expected)

    def test_append_none_to_wrapper_text(self) -> None:
        actual = ET.XML("<div>hello</div>")
        append_text_to_element(actual, None)

        expected = ET.XML("<div>hello</div>")

        self.assertEqual(actual, expected)

    def test_append_text_to_tailless_child(self) -> None:
        actual = ET.XML("<div>hello <i>there</i></div>")
        append_text_to_element(actual, " world")

        expected = ET.XML("<div>hello <i>there</i> world</div>")

        self.assertEqual(actual, expected)

    def test_append_none_to_tailless_child(self) -> None:
        actual = ET.XML("<div>hello <i>there</i></div>")
        append_text_to_element(actual, None)

        expected = ET.XML("<div>hello <i>there</i></div>")

        self.assertEqual(actual, expected)

    def test_append_text_to_child_tail(self) -> None:
        actual = ET.XML("<div>hello <i>there</i> </div>")
        append_text_to_element(actual, "world")

        expected = ET.XML("<div>hello <i>there</i> world</div>")

        self.assertEqual(actual, expected)

    def test_append_none_to_child_tail(self) -> None:
        actual = ET.XML("<div>hello <i>there</i> </div>")
        append_text_to_element(actual, None)

        expected = ET.XML("<div>hello <i>there</i> </div>")

        self.assertEqual(actual, expected)


class TestRemoveFootnoteBackrefs(XMLTestCase):
    def test_basic_operation(self) -> None:
        actual = ET.XML(
            """
            <p xmlns="http://www.bibletechnologies.net/2003/OSIS/namespace">
                <note><reference type="annotateRef">someref</reference>
                    Some note
                </note>
            </p>
            """
        )
        remove_footnote_backrefs(actual)

        expected = ET.XML(
            """
            <p xmlns="http://www.bibletechnologies.net/2003/OSIS/namespace">
                <note>
                    Some note
                </note>
            </p>
            """
        )
        self.assertEqual(actual, expected)

    def test_ignore_refs_outside_a_note(self) -> None:
        actual = ET.XML(
            """
            <p xmlns="http://www.bibletechnologies.net/2003/OSIS/namespace">
                <reference type="annotateRef">someref</reference>
                Some text
            </p>
            """
        )
        remove_footnote_backrefs(actual)

        expected = ET.XML(
            """
            <p xmlns="http://www.bibletechnologies.net/2003/OSIS/namespace">
                <reference type="annotateRef">someref</reference>
                Some text
            </p>
            """
        )
        self.assertEqual(actual, expected)

    def test_ignore_refs_with_different_types(self) -> None:
        actual = ET.XML(
            """
            <p xmlns="http://www.bibletechnologies.net/2003/OSIS/namespace">
                <note><reference type="source">someref</reference>
                    Some note
                </note>
            </p>
            """
        )
        remove_footnote_backrefs(actual)

        expected = ET.XML(
            """
            <p xmlns="http://www.bibletechnologies.net/2003/OSIS/namespace">
                <note><reference type="source">someref</reference>
                    Some note
                </note>
            </p>
            """
        )
        self.assertEqual(actual, expected)
