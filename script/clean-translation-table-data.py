#!/usr/bin/env python3
import argparse
import csv
import logging
import re
import sys
import typing

# Sometimes the TRANSLATION field contains codes
# and ignorable punctuation instead of words.
# To normalise the translation field,
# let's make some less-ambiguous sentinel values.
#
# These constants use only alphanumeric characters
# because we don't want anything to be filtered out.
NOT_TRANSLATED = "NotTranslated"
MERGE_WITH_NEXT = "MergeWithNext"
MERGE_WITH_PREV = "MergeWithPrev"

TRANSLATION_CHAR_RAW_PATTERN = r"[A-Za-z0-9]"
TRANSLATION_CHAR = re.compile(TRANSLATION_CHAR_RAW_PATTERN)
TRANSLATION_WORD = re.compile(TRANSLATION_CHAR_RAW_PATTERN + "+")

# This is not just the negation of TRANSLATION_CHAR_RAW_PATTERN.
NOT_TRANSLATION_CHARS = re.compile(
    r"""
    # Remove commas unless they appear within numbers, like "76,000"
    ,(?![0-9])
    # Remove apostrophes unless they appear before s or t,
    # like "Joshua's" or "don't"
    | ’(?![st])
    # Keep hyphens
    # Keep English translation characters
    # Keep any remaining commas (that *do* appear in numbers)
    # Keep any spaces to separate words
    # Keep any remaining aposttrophes (that *do* appear in contractions)
    # Keep any square brackets (we'll handle them separately)
    | [^-A-Za-z0-9,\ ’[\]]
""",
    re.VERBOSE,
)


class CSVRow(typing.NamedTuple):
    he_ordinal: str
    el_ordinal: str
    en_ordinal: str
    language: str
    verse_ordinal: str
    source_word: str
    ignored1: str
    transliteration: str
    # This grammar does not appear to be the standard(?) Robinson codes,
    # but a custom scheme:
    #
    # https://biblehub.com/hebrewparse.htm
    # https://biblehub.com/abbrev.htm
    grammar_code: str
    grammar_description: str
    strongs_number: str
    reference: str
    heading: str
    cross_references: str
    translation: str
    footnote: str
    gloss: str
    ignored2: str
    ignored3: str
    ignored4: str
    ignored5: str
    ignored6: str
    ignored7: str
    ignored8: str
    ignored9: str
    ignored10: str
    ignored11: str


class TableFixup(typing.NamedTuple):
    old_translation: str
    new_translation: typing.Optional[str] = None
    new_reference: typing.Optional[str] = None
    new_footnote: typing.Optional[str] = None
    new_cross_references: typing.Optional[str] = None


TABLE_FIXUPS = {
    # In Genesis 35:18, Hebrew word 13788 has both a translation "him"
    # and a MERGE_WITH_NEXT marker. That seems to be an error. Word 13788
    # *should* be merged with next (it's the "Ben" in "Ben-oni")
    # and the word "him" should be part of the translation of the previous
    # phrase.
    "13786": TableFixup("she named", "she named him"),
    "13788": TableFixup("vvv him", MERGE_WITH_NEXT),
    # In Numbers 26, there's a lot of numbers where the first word includes
    # the MERGE_WITH_NEXT marker, even though it also has a translation. It
    # looks like the translation is appropriate for the word, so we'll just
    # remove the marker.
    "61153": TableFixup(". . . 40,500", "40,500"),
    "61192": TableFixup(". . . 76,500", "76,500"),
    "61216": TableFixup(". . . 64,300", "64,300"),
    "61237": TableFixup(". . . 60,500", "60,500"),
    "61301": TableFixup(". . . 52,700", "52,700"),
    "61330": TableFixup(". . . 32,500", "32,500"),
    "61372": TableFixup(". . . 45,600", "45,600"),
    "61392": TableFixup(". . . 64,400", "64,400"),
    "61426": TableFixup(". . . 53,400", "53,400"),
    "61451": TableFixup(". . . 45,400", "45,400"),
    # The translation tables mark words added or changed for English
    # readability with [square brackets]. They also use {curly brackets}
    # to represent something I don't quite recognise.
    #
    # In Judges 16:14, the BSB interpolates text found in the Septuagint but
    # not in the Masoretic text. Even though the translators must have been
    # pretty sure this text was supposed to be there, in the translation tables
    # it's marked as an addition in square brackets.
    # In Psalms 145:14, the BSB interpolates text found in the Dead Sea Scrolls,
    # the Syriac translation, and the Septuagint, but not in the Masoretic Text.
    # Here the translators *don'* use square brackets, even though the text
    # they're translating does not appear in the source text they're
    # translating from. I'm going to assume that's an error, because of
    # consistency, and also because it's needed to make this script work.
    "214529": TableFixup(
        "The LORD is faithful in all His words and kind in all His actions .",
        "[The LORD is faithful in all His words and kind in all His actions]",
    ),
    # There are even a number of instances where the translation uses a
    # [mixture} of brackets. Given there's only 11 instances of that
    # in the entire text, I'm going to assume they're errors and need to
    # be corrected.
    #
    # The translated text is a conjuction,
    # so this text is an addition.
    "81372": TableFixup("{He did this] so that", "[He did this] so that"),
    # The literal translation is "against him",
    # so this is an elaboration.
    "145166": TableFixup("against {Amaziah]", "against {Amaziah}"),
    # The literal translation is "to him",
    # so this is an elaboration.
    "261091": TableFixup("to [Baruch} :", "to {Baruch} :"),
    # The literal translation is "this",
    # so this is an addition.
    "328804": TableFixup("{does] this", "[does] this"),
    # The literal translation is "not",
    # so this is an addition.
    "343148": TableFixup("“ [Do} not", "“ [Do] not"),
    # The literal translation is "not",
    # so this is an addition.
    "343692": TableFixup("{do] not", "[do] not"),
    # The literal translation is "his",
    # so this is an addition.
    "371375": TableFixup("{did] His", "[did] His"),
    # The literal translation is "the",
    # so this is an addition.
    "380572": TableFixup("{After] the", "[After] the"),
    # The literal translation is "the",
    # so this is an elaboration.
    "388451": TableFixup("[When}", "[When]"),
    # The literal translation is "the",
    # so this is an addition.
    "414632": TableFixup("{Let] the", "[Let] the"),
    # The literal translation is "not",
    # so this is an addition.
    "427450": TableFixup("{do] not", "[do] not"),
    #
    # The following records have an opening square bracket
    # without a closing square bracket,
    # so we have to guess where it should go.
    #
    # As far as I can tell,
    # there are no records with an unmatched opening curly bracket,
    # or unmatched closing square or curly brackets.
    #
    # This is a verb,
    # so the addition is "the Israelites".
    "83489": TableFixup("[the Israelites learned", "[the Israelites] learned"),
    # The literal translation is "garment",
    # so the addition is "could hold."
    "140467": TableFixup(
        "as his garment [could hold .",
        "as his garment [could hold]",
    ),
    # The literal translation is "rams",
    # so the addition is "as though they were".
    "281098": TableFixup(
        "[as though they were rams ,",
        "[as though they were] rams",
    ),
    # The literal translation of this and the next word
    # (which is marked MERGE_WITH_PREV) is "even days",
    # so "the week" is just an elaboration.
    "388424": TableFixup("[the week[", "{the week}"),
    #
    # Many Psalms have a canonical heading,
    # like "A Psalm of David".
    # Christian tradition is that these headings are *not* included
    # in the first verse,
    # but the translation table data marks them as such.
    # So we'll have to fix that.
    "195607": TableFixup("A Psalm", new_reference=""),
    "195613": TableFixup("O LORD ,", new_reference="Psalm 3:1"),
    "195677": TableFixup("For the choirmaster .", new_reference=""),
    "195681": TableFixup("Answer me", new_reference="Psalm 4:1"),
    "195754": TableFixup("For the choirmaster ,", new_reference=""),
    "195759": TableFixup("Give ear", new_reference="Psalm 5:1"),
    "195865": TableFixup("For the choirmaster .", new_reference=""),
    "195871": TableFixup("O LORD ,", new_reference="Psalm 6:1"),
    "195949": TableFixup("A Shiggaion", new_reference=""),
    "195959": TableFixup("O LORD", new_reference="Psalm 7:1"),
    "196091": TableFixup("For the choirmaster .", new_reference=""),
    "196096": TableFixup("O LORD ,", new_reference="Psalm 8:1"),
    "196168": TableFixup("For the choirmaster", new_reference=""),
    "196173": TableFixup("I will give thanks", new_reference="Psalm 9:1"),
    "196494": TableFixup("For the choirmaster .", new_reference=""),
    "196496": TableFixup("In the LORD", new_reference="Psalm 11:1"),
    "196562": TableFixup("For the choirmaster .", new_reference=""),
    "196567": TableFixup("Help ,", new_reference="Psalm 12:1"),
    "196641": TableFixup("For the choirmaster .", new_reference=""),
    "196644": TableFixup("How long ,", new_reference="Psalm 13:1"),
    "196696": TableFixup("For the choirmaster .", new_reference=""),
    "196698": TableFixup("The fool", new_reference="Psalm 14:1"),
    "196769": TableFixup("A Psalm", new_reference=""),
    "196771": TableFixup("O LORD ,", new_reference="Psalm 15:1"),
    "196824": TableFixup("A Miktam", new_reference=""),
    "196826": TableFixup("Preserve me ,", new_reference="Psalm 16:1"),
    "196921": TableFixup("A prayer", new_reference=""),
    "196923": TableFixup("Hear ,", new_reference="Psalm 17:1"),
    "197045": TableFixup("For the choirmaster .", new_reference=""),
    "197066": TableFixup("I love You ,", new_reference="Psalm 18:1"),
    "197442": TableFixup("For the choirmaster .", new_reference=""),
    "197445": TableFixup("The heavens", new_reference="Psalm 19:1"),
    "197568": TableFixup("For the choirmaster .", new_reference=""),
    "197571": TableFixup("May the LORD", new_reference="Psalm 20:1"),
    "197638": TableFixup("For the choirmaster .", new_reference=""),
    "197641": TableFixup("O LORD ,", new_reference="Psalm 21:1"),
    "197742": TableFixup("For the choirmaster .", new_reference=""),
    "197748": TableFixup("My God ,", new_reference="Psalm 22:1"),
    "197995": TableFixup("A Psalm", new_reference=""),
    "197997": TableFixup("The LORD", new_reference="Psalm 23:1"),
    "198052": TableFixup("A Psalm", new_reference=""),
    "198054": TableFixup("The earth", new_reference="Psalm 24:1"),
    "198141": TableFixup("Of David .", new_reference=""),
    "198142": TableFixup("To You ,", new_reference="Psalm 25:1"),
    "198300": TableFixup("Of David .", new_reference=""),
    "198301": TableFixup("Vindicate me ,", new_reference="Psalm 26:1"),
    "198385": TableFixup("Of David .", new_reference=""),
    "198386": TableFixup("The LORD", new_reference="Psalm 27:1"),
    "198534": TableFixup("Of David .", new_reference=""),
    "198535": TableFixup("To You ,", new_reference="Psalm 28:1"),
    "198630": TableFixup("A Psalm", new_reference=""),
    "198632": TableFixup("Ascribe", new_reference="Psalm 29:1"),
    "198721": TableFixup("A Psalm .", new_reference=""),
    "198726": TableFixup("I will exalt You ,", new_reference="Psalm 30:1"),
    "198818": TableFixup("For the choirmaster .", new_reference=""),
    "198821": TableFixup("In You ,", new_reference="Psalm 31:1"),
    "199038": TableFixup("Of David .", new_reference=""),
    "199040": TableFixup("Blessed is he", new_reference="Psalm 32:1"),
    "199309": TableFixup("Of David ,", new_reference=""),
    "199317": TableFixup("I will bless", new_reference="Psalm 34:1"),
    "199474": TableFixup("Of David .", new_reference=""),
    "199475": TableFixup("Contend", new_reference="Psalm 35:1"),
    "199703": TableFixup("For the choirmaster .", new_reference=""),
    "199707": TableFixup("An oracle", new_reference="Psalm 36:1"),
    "199803": TableFixup("Of David .", new_reference=""),
    "199804": TableFixup("Do not", new_reference="Psalm 37:1"),
    "200101": TableFixup("A Psalm", new_reference=""),
    "200104": TableFixup("O LORD ,", new_reference="Psalm 38:1"),
    "200269": TableFixup("For the choirmaster .", new_reference=""),
    "200273": TableFixup("I said ,", new_reference="Psalm 39:1"),
    "200398": TableFixup("For the choirmaster .", new_reference=""),
    "200401": TableFixup("I waited patiently for", new_reference="Psalm 40:1"),
    "200583": TableFixup("For the choirmaster .", new_reference=""),
    "200586": TableFixup("Blessed", new_reference="Psalm 41:1"),
    "200702": TableFixup("For the choirmaster .", new_reference=""),
    "200706": TableFixup("As the deer", new_reference="Psalm 42:1"),
    "200893": TableFixup("For the choirmaster .", new_reference=""),
    "200897": TableFixup("We have heard", new_reference="Psalm 44:1"),
    "201091": TableFixup("For the choirmaster .", new_reference=""),
    "201099": TableFixup("My heart", new_reference="Psalm 45:1"),
    "201251": TableFixup("For the choirmaster .", new_reference=""),
    "201257": TableFixup("God", new_reference="Psalm 46:1"),
    "201351": TableFixup("For the choirmaster .", new_reference=""),
    "201355": TableFixup("Clap", new_reference="Psalm 47:1"),
    "201428": TableFixup("A song .", new_reference=""),
    "201432": TableFixup("Great is", new_reference="Psalm 48:1"),
    "201539": TableFixup("For the choirmaster .", new_reference=""),
    "201543": TableFixup("Hear", new_reference="Psalm 49:1"),
    "201706": TableFixup("A Psalm", new_reference=""),
    "201708": TableFixup("The Mighty One ,", new_reference="Psalm 50:1"),
    "201884": TableFixup("For the choirmaster .", new_reference=""),
    "201896": TableFixup("Have mercy on me ,", new_reference="Psalm 51:1"),
    "202037": TableFixup("For the choirmaster .", new_reference=""),
    "202052": TableFixup("Why", new_reference="Psalm 52:1"),
    "202127": TableFixup("For the choirmaster .", new_reference=""),
    "202132": TableFixup("The fool", new_reference="Psalm 53:1"),
    "202204": TableFixup("For the choirmaster .", new_reference=""),
    "202216": TableFixup("Save me ,", new_reference="Psalm 54:1"),
    "202266": TableFixup("For the choirmaster .", new_reference=""),
    "202270": TableFixup("Listen", new_reference="Psalm 55:1"),
    "202458": TableFixup("For the choirmaster .", new_reference=""),
    "202469": TableFixup("Be merciful to me ,", new_reference="Psalm 56:1"),
    "202578": TableFixup(
        "For the choirmaster. [To the tune of]", new_reference=""
    ),
    "202587": TableFixup("Have mercy on me ,", new_reference="Psalm 57:1"),
    "202684": TableFixup("For the choirmaster .", new_reference=""),
    "202689": TableFixup("Do you indeed", new_reference="Psalm 58:1"),
    "202784": TableFixup("For the choirmaster .", new_reference=""),
    "202795": TableFixup("Deliver me", new_reference="Psalm 59:1"),
    "202940": TableFixup("For the choirmaster .", new_reference=""),
    "202964": TableFixup("You have rejected us ,", new_reference="Psalm 60:1"),
    "203053": TableFixup("For the choirmaster .", new_reference=""),
    "203057": TableFixup("Hear", new_reference="Psalm 61:1"),
    "203121": TableFixup("For the choirmaster .", new_reference=""),
    "203126": TableFixup("In", new_reference="Psalm 62:1"),
    "203238": TableFixup("A Psalm", new_reference=""),
    "203243": TableFixup("O God ,", new_reference="Psalm 63:1"),
    "203331": TableFixup("For the choirmaster .", new_reference=""),
    "203334": TableFixup("Hear ,", new_reference="Psalm 64:1"),
    "203413": TableFixup("For the choirmaster .", new_reference=""),
    "203417": TableFixup("Praise", new_reference="Psalm 65:1"),
    "203522": TableFixup("For the choirmaster .", new_reference=""),
    "203525": TableFixup("Make a joyful noise", new_reference="Psalm 66:1"),
    "203676": TableFixup("For the choirmaster .", new_reference=""),
    "203680": TableFixup("May God", new_reference="Psalm 67:1"),
    "203729": TableFixup("For the choirmaster .", new_reference=""),
    "203733": TableFixup("God", new_reference="Psalm 68:1"),
    "204039": TableFixup("For the choirmaster .", new_reference=""),
    "204043": TableFixup("Save me ,", new_reference="Psalm 69:1"),
    "204330": TableFixup("For the choirmaster .", new_reference=""),
    "204333": TableFixup("[Make haste], O God ,", new_reference="Psalm 70:1"),
    "204580": TableFixup("Of Solomon .", new_reference=""),
    "204581": TableFixup("Endow", new_reference="Psalm 72:1"),
    "204742": TableFixup("A Psalm", new_reference=""),
    "204744": TableFixup("Surely", new_reference="Psalm 73:1"),
    "204935": TableFixup("A Maskil", new_reference=""),
    "204937": TableFixup("Why", new_reference="Psalm 74:1"),
    "205130": TableFixup("For the choirmaster :", new_reference=""),
    "205136": TableFixup("We give thanks", new_reference="Psalm 75:1"),
    "205217": TableFixup("For the choirmaster .", new_reference=""),
    "205222": TableFixup("God", new_reference="Psalm 76:1"),
    "205307": TableFixup("For the choirmaster .", new_reference=""),
    "205312": TableFixup("I cried out", new_reference="Psalm 77:1"),
    "205461": TableFixup("A Maskil", new_reference=""),
    "205463": TableFixup("Give ear ,", new_reference="Psalm 78:1"),
    "205991": TableFixup("A Psalm", new_reference=""),
    "205993": TableFixup("The nations ,", new_reference="Psalm 79:1"),
    "206123": TableFixup("For the choirmaster .", new_reference=""),
    "206129": TableFixup("Hear us ,", new_reference="Psalm 80:1"),
    "206264": TableFixup("For the choirmaster .", new_reference=""),
    "206268": TableFixup("Sing for joy", new_reference="Psalm 81:1"),
    "206389": TableFixup("A Psalm", new_reference=""),
    "206391": TableFixup("God", new_reference="Psalm 82:1"),
    "206450": TableFixup("A song .", new_reference=""),
    "206453": TableFixup("O God ,", new_reference="Psalm 83:1"),
    "206580": TableFixup("For the choirmaster .", new_reference=""),
    "206586": TableFixup("How", new_reference="Psalm 84:1"),
    "206696": TableFixup("For the choirmaster .", new_reference=""),
    "206700": TableFixup("You showed favor", new_reference="Psalm 85:1"),
    "206792": TableFixup("A prayer", new_reference=""),
    "206794": TableFixup("Incline", new_reference="Psalm 86:1"),
    "206939": TableFixup("A Psalm", new_reference=""),
    "206943": TableFixup("He has founded His city", new_reference="Psalm 87:1"),
    "206993": TableFixup("A song .", new_reference=""),
    "207004": TableFixup("O LORD ,", new_reference="Psalm 88:1"),
    "207135": TableFixup("A Maskil", new_reference=""),
    "207138": TableFixup("I will sing", new_reference="Psalm 89:1"),
    "207519": TableFixup("A prayer", new_reference=""),
    "207523": TableFixup("Lord ,", new_reference="Psalm 90:1"),
    "207771": TableFixup("A Psalm .", new_reference=""),
    "207775": TableFixup("It is good", new_reference="Psalm 92:1"),
    "208393": TableFixup("A Psalm .", new_reference=""),
    "208394": TableFixup("Sing", new_reference="Psalm 98:1"),
    "208551": TableFixup("A Psalm", new_reference=""),
    "208553": TableFixup("Make a joyful noise", new_reference="Psalm 100:1"),
    "208594": TableFixup("A Psalm", new_reference=""),
    "208596": TableFixup("I will sing", new_reference="Psalm 101:1"),
    "208677": TableFixup("A prayer", new_reference=""),
    "208685": TableFixup("Hear", new_reference="Psalm 102:1"),
    "208890": TableFixup("Of David .", new_reference=""),
    "208891": TableFixup("Bless", new_reference="Psalm 103:1"),
    "210230": TableFixup("A song .", new_reference=""),
    "210233": TableFixup("My heart", new_reference="Psalm 108:1"),
    "210329": TableFixup("For the choirmaster .", new_reference=""),
    "210332": TableFixup("O God", new_reference="Psalm 109:1"),
    "210556": TableFixup("A Psalm", new_reference=""),
    "210558": TableFixup("The LORD", new_reference="Psalm 110:1"),
    "212431": TableFixup("A song", new_reference=""),
    "212433": TableFixup("In my distress", new_reference="Psalm 120:1"),
    "212482": TableFixup("A song", new_reference=""),
    "212484": TableFixup("I lift up", new_reference="Psalm 121:1"),
    "212538": TableFixup("A song", new_reference=""),
    "212541": TableFixup("I was glad", new_reference="Psalm 122:1"),
    "212600": TableFixup("A song", new_reference=""),
    "212602": TableFixup("I lift up", new_reference="Psalm 123:1"),
    "212641": TableFixup("A song", new_reference=""),
    "212644": TableFixup("If the LORD", new_reference="Psalm 124:1"),
    "212698": TableFixup("A song", new_reference=""),
    "212700": TableFixup("Those who trust", new_reference="Psalm 125:1"),
    "212747": TableFixup("A song", new_reference=""),
    "212749": TableFixup("When the LORD", new_reference="Psalm 126:1"),
    "212797": TableFixup("A song", new_reference=""),
    "212800": TableFixup("Unless", new_reference="Psalm 127:1"),
    "212857": TableFixup("A song", new_reference=""),
    "212859": TableFixup("Blessed", new_reference="Psalm 128:1"),
    "212904": TableFixup("A song", new_reference=""),
    "212906": TableFixup("Many a time", new_reference="Psalm 129:1"),
    "212958": TableFixup("A song", new_reference=""),
    "212960": TableFixup("Out of the depths", new_reference="Psalm 130:1"),
    "213012": TableFixup("A song", new_reference=""),
    "213015": TableFixup("My heart", new_reference="Psalm 131:1"),
    "213045": TableFixup("A song", new_reference=""),
    "213047": TableFixup("O LORD ,", new_reference="Psalm 132:1"),
    "213176": TableFixup("A song", new_reference=""),
    "213179": TableFixup("Behold ,", new_reference="Psalm 133:1"),
    "213216": TableFixup("A song", new_reference=""),
    "213218": TableFixup("Come ,", new_reference="Psalm 134:1"),
    "213658": TableFixup("Of David .", new_reference=""),
    "213659": TableFixup("I give You thanks", new_reference="Psalm 138:1"),
    "213734": TableFixup("For the choirmaster .", new_reference=""),
    "213737": TableFixup("O LORD ,", new_reference="Psalm 139:1"),
    "213911": TableFixup("For the choirmaster .", new_reference=""),
    "213914": TableFixup("Rescue me ,", new_reference="Psalm 140:1"),
    "214027": TableFixup("A Psalm", new_reference=""),
    "214029": TableFixup("I call upon You ,", new_reference="Psalm 141:1"),
    "214122": TableFixup("A Maskil", new_reference=""),
    "214127": TableFixup("I cry", new_reference="Psalm 142:1"),
    "214197": TableFixup("A Psalm", new_reference=""),
    "214199": TableFixup("O LORD ,", new_reference="Psalm 143:1"),
    "214314": TableFixup("Of David .", new_reference=""),
    "214315": TableFixup("Blessed", new_reference="Psalm 144:1"),
    "214444": TableFixup("[A Psalm] of praise .", new_reference=""),
    "214446": TableFixup("I will exalt You ,", new_reference="Psalm 145:1"),
    #
    # In Proverbs 18:5, the original USFM text says "Showing partiality",
    # and then has a footnote saying that this is an interpretation of the
    # Hebrew idiom "lifting the face". In the translation tables, we have the
    # same text but the footnote is after a word with the MERGE_WITH_NEXT
    # marker. This confuses things and we don't encounter the footnote when we
    # expect to.
    # The fix is to move the footnote to just after the text it describes.
    "218733": TableFixup(
        "Showing partiality", new_footnote="Hebrew <i>Lifting the face</i>"
    ),
    "218734": TableFixup("vvv", new_footnote=""),
    # In 1 Chronicles 6:27, a footnote has slightly mangled markup, missing an
    # opening tag before "Elihu". Let's fix it.
    "153088": TableFixup(
        "Eliab",
        new_footnote="<i>Eliab</i> is also called <i>Eliel</i>; see verse 34. "
        "Both of these are other names for <i>Elihu</i>; see 1 Samuel 1:1.",
    ),
    # ...and again in verse 34.
    "153159": TableFixup(
        "of Eliel ,",
        new_footnote="<i>Eliel</i> is also called <i>Eliab</i>; see verse 27. "
        "Both of these are other names for <i>Elihu</i>; see 1 Samuel 1:1.",
    ),
    # In 1 Chronicles 7:28, a footnote has slightly mangled markup, using two
    # closing tags around "Gaza" instead of a matched pair.
    "153959": TableFixup(
        "Ayyah",
        new_footnote="<i>Ayyah</i> is another name for <i>Gaza</i>; "
        "see also LXX.",
    ),
    #
    # In Malachi 2:18, the translation table has an extra space after the
    # opening bracket of the cross-reference text, so let's patch it.
    "305129": TableFixup(
        "“ Behold ,", new_cross_references="(Matthew 11:7–19; Luke 7:24–35)"
    ),
}


def cleanup_table_data(
    rows: typing.Iterator[CSVRow],
) -> typing.Iterator[CSVRow]:
    for row in rows:
        if row.en_ordinal == "":
            # Skip over the spacing rows between verses
            continue

        translation = row.translation.strip()

        # There's some data in the table that just doesn't make sense.
        # Before we try to decode anything, let's make sure our known
        # fixes are safely applied.
        fixup = TABLE_FIXUPS.get(row.en_ordinal)
        if fixup is not None:
            if translation != fixup.old_translation:
                logging.warning(
                    "Couldn't apply fixup for English word %s, "
                    "got translation %r, "
                    "expected %r",
                    row.en_ordinal,
                    translation,
                    fixup.old_translation,
                )
            else:
                if fixup.new_translation is not None:
                    translation = fixup.new_translation
                if fixup.new_reference is not None:
                    row = row._replace(reference=fixup.new_reference)
                if fixup.new_footnote is not None:
                    row = row._replace(footnote=fixup.new_footnote)
                if fixup.new_cross_references is not None:
                    row = row._replace(
                        cross_references=fixup.new_cross_references
                    )

        # The translation table spreadsheet is trying to solve
        # a very difficult problem:
        # mapping zero-or-more Hebrew/Greek source words
        # to zero-or-more English target words.
        # This shouldn't really fit into a spreadsheet,
        # but they make it mostly work
        # by putting special codes into the "English translation" field.
        if "vvv" in translation:
            # Double-check there's no other translatable text in this row.
            assert (
                TRANSLATION_CHAR.search(translation.replace("vvv", "")) is None
            ), translation

            # "vvv" means this source word is included in
            # the translation of the following word (I think).
            # I don't know why they didn't just put the English translation
            # in the first row and use the existing ". . ." marker.
            translation = MERGE_WITH_NEXT

        elif ". . ." in translation:
            # Double-check there's no other translatable text in this row.
            assert TRANSLATION_CHAR.search(translation) is None, translation
            # ". . ." means this source word included in
            # the translation of the previous word (I think).
            translation = MERGE_WITH_PREV

        elif TRANSLATION_CHAR.search(translation) is None:
            # This covers the case of words
            # specifically marked as untranslated with "-",
            # as well as words that are just mapped to punctuation
            # or an empty string.
            #
            # The "remove the extra punctuation" step below
            # keeps "-" characters,
            # since they occur in compound words we watn to keep.
            # This test does *not* count "-" as an important character,
            # to catch "translations" like "' -" or "- ?".
            translation = NOT_TRANSLATED

        # Since this data comes from a spreadsheet, Excel likes to
        # store things that look like numbers as numbers, turning
        # things like commas into formatting details.
        # Unfortunately, this formatting is lost when we export to CSV,
        # and the OSIS text *does* contain commas,
        # so we'll need to put them back.
        elif translation.isdecimal():
            translation = "{0:,d}".format(int(translation))

        assert "vvv" not in translation
        assert ". . ." not in translation

        # Remove the extra punctuation characters
        # that we don't want to match against our XML text.
        # This must be done after detecting obvious tokens above,
        # because many of those tokens consist of
        # non-translation characters.
        translation = NOT_TRANSLATION_CHARS.sub(" ", translation).strip()

        yield row._replace(translation=translation)


def normalise_table_merge_data(
    rows: typing.Iterator[CSVRow],
) -> typing.Iterator[CSVRow]:
    last_row = next(rows)
    for row in rows:
        if row.translation == MERGE_WITH_PREV:
            if last_row.translation in (MERGE_WITH_NEXT, NOT_TRANSLATED):
                # There's a few instances of a MERGE_WITH_NEXT row
                # being immediately followed by MERGE_WITH_PREV:
                #
                # - Genesis 26:13
                # - Judges 3:2
                # - Ezekiel 41:26
                # - Ezekiel 47:8
                # - Ezekiel 48:1
                # - Acts 8:6
                #
                # Upon inspection they generally seem like
                # word pairs necessary in the source language
                # that aren't necessary in English.
                # I say we just mark them all NOT_TRANSLATED.
                last_row = last_row._replace(translation=NOT_TRANSLATED)
                row = row._replace(translation=NOT_TRANSLATED)
            else:
                # Instead of merging this with the previous row,
                # the state machine is simpler if we can merge the previous row
                # with this one.
                row = row._replace(translation=last_row.translation)
                last_row = last_row._replace(translation=MERGE_WITH_NEXT)

                if last_row.footnote:
                    if row.footnote:
                        row = row._replace(
                            footnote=last_row.footnote + " " + row.footnote
                        )
                    else:
                        row = row._replace(footnote=last_row.footnote)
                    last_row = last_row._replace(footnote="")

        yield last_row
        last_row = row

    yield last_row


def main(raw_args: typing.List[str]) -> int:
    parser = argparse.ArgumentParser(
        description="Clean up the translation table data"
    )
    parser.add_argument(
        "--verbose",
        "-v",
        dest="verbose",
        default=0,
        action="count",
        help="Show more details; can be repeated.",
    )
    parser.add_argument(
        "input",
        metavar="INPUT",
        help="The raw CSV file converted from the original XLSX",
        type=argparse.FileType("r"),
    )
    parser.add_argument(
        "output",
        metavar="OUTPUT",
        help="Write the updated CSV data to this file (must not exist yet)",
        type=argparse.FileType("x"),
    )
    args = parser.parse_args(raw_args[1:])

    logging.basicConfig(
        format="%(levelname)s: %(message)s",
        level=logging.WARNING - (args.verbose * 10),
    )
    logging.captureWarnings(True)

    with args.input as input, args.output as output:
        input_lines = iter(input)

        # Skip the copyright line and headers
        next(input_lines)
        next(input_lines)

        # Read each row into a tuple
        cleaned_rows: typing.Iterator[CSVRow] = (
            CSVRow(*row) for row in csv.reader(input_lines)
        )
        cleaned_rows = cleanup_table_data(cleaned_rows)
        cleaned_rows = normalise_table_merge_data(cleaned_rows)

        writer = csv.writer(output)
        writer.writerow(
            (
                "he_ordinal",
                "el_ordinal",
                "en_ordinal",
                "language",
                "verse_ordinal",
                "source_word",
                "transliteration",
                "grammar_code",
                "grammar_description",
                "strongs_number",
                "reference",
                "heading",
                "cross_references",
                "translation",
                "footnote",
                "gloss",
            )
        )

        for each in cleaned_rows:
            writer.writerow(
                (
                    each.he_ordinal,
                    each.el_ordinal,
                    each.en_ordinal,
                    each.language,
                    each.verse_ordinal,
                    each.source_word,
                    each.transliteration,
                    each.grammar_code,
                    each.grammar_description,
                    each.strongs_number,
                    each.reference,
                    each.heading,
                    each.cross_references,
                    each.translation,
                    each.footnote,
                    each.gloss,
                )
            )

    return 0


if __name__ == "__main__":
    sys.exit(main(sys.argv))
