#!/bin/sh
# Update our vendored scripts to the latest available upstream version.

die() { printf "%s\n" "$*" >&2; exit 1; }

cd "$(dirname "$0")"/.. || die "Cannot change directory to repsitory root."

git subtree --prefix script/adyeths/u2o pull https://github.com/adyeths/u2o.git master
git subtree --prefix script/Screwtapello/osis-decorators/ pull https://gitlab.com/Screwtapello/osis-decorators.git main
git subtree --prefix script/dilshod/xlsx2csv pull https://github.com/dilshod/xlsx2csv.git master
